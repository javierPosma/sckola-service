package com.posma.sckola.app.aspects;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@Aspect
public class PerformanceAspect {

    private static final Logger LOGGER = Logger.getLogger("performance");

    /**
     * Esta funcion se encarga de calcular el tiempo de ejecucion de una funcion del controller
     * @param joinPoint punto de intercepcion de la funcion
     * @return objeto que retorna la funcion
     * @throws Throwable
     */
    @Around(" execution(* com.posma.sckola.app.controller.*.*(..))")
    public Object performanceAround(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long elapsedTime = System.currentTimeMillis() - startTime;
        LOGGER.info("Class: "+ joinPoint.getTarget().getClass().getName()+", Method: "+ joinPoint.getSignature().getName()+ ",  Execution Time: " + elapsedTime + " milliseconds.");
        return result;
    }

    /**
     * Funcion que se encarga de calcular la memoria utilizada despues de ejecutar el metodo perteneciente
     * a el package controllers
     * @throws Throwable
     */
    @After("execution(* com.posma.sckola.app.controller.*.*(..))")
    public void logMemory() throws  Throwable{
        LOGGER.info("JVM memory in use = " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()));
    }

}
