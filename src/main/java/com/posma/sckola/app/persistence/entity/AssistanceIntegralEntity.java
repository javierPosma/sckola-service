package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 18/04/2017.
 */
@Entity
@Table(name = "ASSISTANCE_INTEGRAL",
        indexes = {
                @Index(columnList ="USER_ID",name = "IDX_USER_ID_ASSISTANCE_INTEGRAL"),
                @Index(columnList ="SECTION_ID",name = "IDX_SECTION_ID_ASSISTANCE_INTEGRAL"),
                @Index(columnList ="DATE",name = "IDX_DATE_ASSISTANCE_INTEGRAL")
        })
public class AssistanceIntegralEntity implements Serializable {

    @Id
    @Embedded
    private AssistanceIntegralPk id;

    @Column(name = "ATTENDED", nullable=false)
    private Boolean attended;

    // no es un campo com tal, solo crea relacion (contraint) de Foreign key
    @ManyToOne
    @JoinColumn(name = "USER_ID", insertable = false, updatable = false)
    private UserEntity user;

    // no es un campo com tal, solo crea relacion (contraint) de Foreign key
    @ManyToOne
    @JoinColumn(name = "SECTION_ID", insertable = false, updatable = false)
    private SectionEntity sectionId;

    public AssistanceIntegralEntity() {

    }

    public AssistanceIntegralEntity(AssistanceIntegralPk id, Boolean attended) {
        this.id = id;
        this.attended = attended;
    }


    public AssistanceIntegralPk getId() {
        return id;
    }

    public void setId(AssistanceIntegralPk id) {
        this.id = id;
    }

    public Boolean getAttended() {
        return attended;
    }

    public void setAttended(Boolean attended) {
        this.attended = attended;
    }

}
