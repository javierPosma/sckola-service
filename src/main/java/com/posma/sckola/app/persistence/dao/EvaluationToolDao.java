package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.EvaluationToolEntity;

import java.util.List;

/**
 * Created by Francis Ries on 12/04/2017.
 */
public interface EvaluationToolDao {

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @return EvaluationToolEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationToolEntity findOne(long id);

    /**
     * description
     * @since 12/04/2017
     * @return List<EvaluationToolEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationToolEntity> findAll();

    /**
     * description
     * @since 12/04/2017
     * @param evaluationTool
     * @return EvaluationToolEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationToolEntity create(EvaluationToolEntity evaluationTool);

    /**
     * description
     * @since 12/04/2017
     * @param evaluationTool
     * @return EvaluationToolEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationToolEntity update(EvaluationToolEntity evaluationTool);

    /**
     * description
     * @since 12/04/2017
     * @param evaluationTool
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(EvaluationToolEntity evaluationTool);

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);
}
