package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.EvaluationScaleTemplateEntity;

import java.util.List;

/**
 * Created by Francis Ries on 27/04/2017.
 */
public interface EvaluationScaleTemplateDao {

    /**
     * description
     * @since 27/04/2017
     * @param id evaluation identifier
     * @return EvaluationScaleTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationScaleTemplateEntity findOne(long id);

    /**
     * description
     * @since 27/04/2017
     * @return List<EvaluationScaleTemplateEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationScaleTemplateEntity> findAll();

    /**
     * description
     * @since 27/04/2017
     * @param evaluationScaleTemplate
     * @return EvaluationScaleTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationScaleTemplateEntity create(EvaluationScaleTemplateEntity evaluationScaleTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param evaluationScaleTemplate
     * @return EvaluationScaleTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationScaleTemplateEntity update(EvaluationScaleTemplateEntity evaluationScaleTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param evaluationScaleTemplate
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(EvaluationScaleTemplateEntity evaluationScaleTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);
}
