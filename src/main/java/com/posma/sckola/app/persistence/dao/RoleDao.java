package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.RoleEntity;

import java.util.List;

/**
 * Created by Francis Ries on 18/04/2017.
 */
public interface RoleDao {

    /**
     * description
     * @since 18/04/2017
     * @param id role identifier
     * @return RoleEntity
     * @author PosmaGroup
     * @version 1.0
     */
    RoleEntity findOne(long id);

    /**
     * description
     * @since 18/04/2017
     * @return List<RoleEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<RoleEntity> findAll();

    /**
     * description
     * @since 18/04/2017
     * @param role
     * @return RoleEntity
     * @author PosmaGroup
     * @version 1.0
     */
    RoleEntity create(RoleEntity role);

    /**
     * description
     * @since 18/04/2017
     * @param role
     * @return RoleEntity
     * @author PosmaGroup
     * @version 1.0
     */
    RoleEntity update(RoleEntity role);

    /**
     * description
     * @since 18/04/2017
     * @param role
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(RoleEntity role);

    /**
     * description
     * @since 18/04/2017
     * @param id role identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

}
