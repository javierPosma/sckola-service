package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.EvaluationValueTemplateEntity;

import java.util.List;

/**
 * Created by Francis Ries on 27/04/2017.
 */
public interface EvaluationValueTemplateDao {

    /**
     * description
     * @since 27/04/2017
     * @param id evaluation identifier
     * @return EvaluationValueTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationValueTemplateEntity findOne(long id);

    /**
     * description
     * @since 27/04/2017
     * @return List<EvaluationValueTemplateEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationValueTemplateEntity> findAll();

    /**
     * description
     * @since 27/04/2017
     * @param evaluationValueTemplate
     * @return EvaluationValueTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationValueTemplateEntity create(EvaluationValueTemplateEntity evaluationValueTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param evaluationValueTemplate
     * @return EvaluationValueTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationValueTemplateEntity update(EvaluationValueTemplateEntity evaluationValueTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param evaluationValueTemplate
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(EvaluationValueTemplateEntity evaluationValueTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);
}
