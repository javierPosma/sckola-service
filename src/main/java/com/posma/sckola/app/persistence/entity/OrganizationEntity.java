package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@Entity
@Table(name="ORGANIZATION")
public class OrganizationEntity implements Serializable {
    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "ORGANIZATION_SEQ", sequenceName = "S_ID_ORGANIZATION", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORGANIZATION_SEQ")
    private Long id;

    @Column(name = "TAX_IDENTITY", nullable=false, length = 30)
    private String taxIdentity;

    @Column(name = "BUSINESS_NAME", nullable=false, length = 35, unique = true)
    private String businessName;

    @Column(name = "EMAIL", nullable=true)
    private String email;

    @Column(name = "ADDRESS", nullable=true, length = 255)
    private String address;

    @OneToMany(targetEntity = PhoneEntity.class, mappedBy = "organization")
    private List<PhoneEntity> phoneList = new ArrayList<PhoneEntity>();


    public OrganizationEntity() { }

    public OrganizationEntity(String address, String businessName, String email, String taxIdentity) {
        this.address = address;
        this.businessName = businessName;
        this.email = email;
        this.taxIdentity = taxIdentity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaxIdentity() {
        return taxIdentity;
    }

    public void setTaxIdentity(String taxIdentity) {
        this.taxIdentity = taxIdentity;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PhoneEntity> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<PhoneEntity> users) {
        this.phoneList = phoneList;
    }

    public boolean addPhoneList(PhoneEntity user) {
        return phoneList.add(user);
    }

}
