package com.posma.sckola.app.persistence.entity;


/**
 * Created by Francis on 24/04/2017.
 *
 * ACTIVE: clase activa
 * INACTIVE: clase inactiva
 *
 */

public enum StatusSection {
    ACTIVE,INACTIVE
}
