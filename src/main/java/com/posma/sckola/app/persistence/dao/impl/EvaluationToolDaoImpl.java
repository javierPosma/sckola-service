package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.EvaluationToolDao;
import com.posma.sckola.app.persistence.entity.EvaluationToolEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 12/04/2017.
 */

@Repository("EvaluationToolDao")
public class EvaluationToolDaoImpl extends AbstractDao<EvaluationToolEntity> implements EvaluationToolDao {

    public EvaluationToolDaoImpl(){
        super ();
        setClazz(EvaluationToolEntity.class);
    }


}
