package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 27/04/2017.
 */
@Entity
@Table(name = "EVALUATION_PLAN_TEMPLATE")
public class EvaluationPlanTemplateEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "EVALUATION_PLAN_TEMPLATE_SEQ", sequenceName = "S_ID_EVALUATION_PLAN_TEMPLATE", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVALUATION_PLAN_TEMPLATE_SEQ")
    private Long id;

    @Column(name = "NAME", nullable=false, length = 80)
    private String name;

    @OneToMany(mappedBy="evaluationPlanTemplate", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<EvaluationTemplateEntity> evaluationTemplateList = new ArrayList<>();
    

    @Column(name = "STATUS", nullable = false)
    private Status status = Status.ACTIVE;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private UserEntity user;


    public EvaluationPlanTemplateEntity() {

    }

    public EvaluationPlanTemplateEntity(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EvaluationTemplateEntity> getEvaluationTemplateList() {
        return evaluationTemplateList;
    }

    public void setEvaluationTemplateList(List<EvaluationTemplateEntity> evaluationTemplateList) {
        this.evaluationTemplateList = evaluationTemplateList;
    }

    public boolean addEvaluationTemplateList(EvaluationTemplateEntity evaluation) {
        return evaluationTemplateList.add(evaluation);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }


}
