package com.posma.sckola.app.persistence.dao;


import com.posma.sckola.app.persistence.entity.OrganizationEntity;

import java.util.List;

/**
 * Created by Francis on 27/03/2017.
 */
public interface OrganizationDao {

    /**
     * description 
     * @since 27/03/2017
     * @param id organization identifier
     * @return OrganizationEntity
     * @author PosmaGroup
     * @version 1.0
     */
    OrganizationEntity findOne(long id);
    
    /**
     * description 
     * @since 27/03/2017
     * @return List<OrganizationEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<OrganizationEntity> findAll();

    /**
     * description 
     * @since 27/03/2017
     * @param fieldName column name
     * @param id organization identifier
     * @return List<OrganizationEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<OrganizationEntity> findAllByFieldId(String fieldName, long id);


    /**
     * description 
     * @since 27/03/2017
     * @param organization
     * @return OrganizationEntity
     * @author PosmaGroup
     * @version 1.0
     */
    OrganizationEntity create(OrganizationEntity organization);

    /**
     * description 
     * @since 27/03/2017
     * @param organization
     * @return OrganizationEntity
     * @author PosmaGroup
     * @version 1.0
     */
    OrganizationEntity update(OrganizationEntity organization);

    /**
     * description 
     * @since 27/03/2017
     * @param organization
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(OrganizationEntity organization);

    /**
     * description
     * @since 27/03/2017
     * @param id organization identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

    /**
     * description
     * @since 27/03/2017
     * @param businessName Business Name
     * @author Francis Ries
     * @version 1.0
     */
    OrganizationEntity findByBusinessName(String businessName);
}
