package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.MatterCommunitySectionDao;
import com.posma.sckola.app.persistence.entity.MatterCommunitySectionEntity;
import com.posma.sckola.app.persistence.entity.SectionType;
import com.posma.sckola.app.persistence.entity.StatusClass;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Francis Ries on 21/04/2017.
 */
@Repository("MatterCommunitySectionDao")
public class MatterCommunitySectionDaoImpl extends AbstractDao<MatterCommunitySectionEntity> implements MatterCommunitySectionDao {

    public MatterCommunitySectionDaoImpl(){
        super();
        setClazz(MatterCommunitySectionEntity.class);
    }


    /**
     * description
     * @since 12/04/2017
     * @return List<MatterCommunitySectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    @Override
    public List<MatterCommunitySectionEntity> findAllMatterCommunitySectionByTeacher(long communityId, long userId, StatusClass status){
        Query query = this.getEntityManager().createQuery("SELECT m FROM MatterCommunitySectionEntity as m WHERE  m.user.id = :userId " +
                "AND m.matterCommunity.community.id = :communityId " +
                "AND m.status = :status");
        query.setParameter("userId", userId);
        query.setParameter("communityId", communityId);
        query.setParameter("status", status);
        return query.getResultList();
    }

    /**
     * description
     * @since 12/04/2017
     * @return List<MatterCommunitySectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    @Override
    public List<MatterCommunitySectionEntity> findAllMatterCommunitySectionByTeacherWithoutEvaluationPlan(long communityId, long userId, StatusClass status){
        Query query = this.getEntityManager().createQuery("SELECT m FROM MatterCommunitySectionEntity as m WHERE  m.user.id = :userId " +
                "AND m.matterCommunity.community.id = :communityId " +
                "AND m.status = :status AND m.evaluationPlan is null");
        query.setParameter("userId", userId);
        query.setParameter("communityId", communityId);
        query.setParameter("status", status);
        return query.getResultList();
    }


    /**
     * description consultar todas las materias activas de un profesor, en todas las comunidades
     * @since 17/05/2017
     * @return List<MatterCommunitySectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    @Override
    public List<MatterCommunitySectionEntity> findAllMatterCommunitySectionByTeacher(long userId, StatusClass status){
        Query query = this.getEntityManager().createQuery("SELECT m FROM MatterCommunitySectionEntity as m WHERE  " +
                "m.user.id = :userId " +
                "AND m.status = :status");
        query.setParameter("userId", userId);
        query.setParameter("status", status);
        return query.getResultList();
    }

    /**
     * description
     * @since 21/04/2017
     * @return MatterCommunitySectionEntity
     * @author PosmaGroup
     * @version 1.0
     */
    @Override
    public MatterCommunitySectionEntity findMatterCommunitySectionUser(long matterCommunityId, long sectionId, long userId, StatusClass status){
        Query query = this.getEntityManager().createQuery("SELECT m FROM MatterCommunitySectionEntity as m WHERE  m.user.id = :userId " +
                "AND m.matterCommunity.id = :matterCommunityId " +
                "AND m.section.id = :sectionId " +
                "AND m.status = :status");
        query.setParameter("matterCommunityId", matterCommunityId);
        query.setParameter("sectionId", sectionId);
        query.setParameter("userId", userId);
        query.setParameter("status", status);
        List<MatterCommunitySectionEntity> matterCommunitySectionEntities = query.getResultList();

        return matterCommunitySectionEntities.size() > 0? matterCommunitySectionEntities.get(0):null;
    }

    /**
     * description
     * @since 12/04/2017
     * @return List<MatterCommunitySectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    @Override
    public List<MatterCommunitySectionEntity> findAllMatterCommunitySectionByTeacherByType(long communityId, long userId, StatusClass status, SectionType sectionType){
        Query query = this.getEntityManager().createQuery("SELECT m FROM MatterCommunitySectionEntity as m WHERE  m.user.id = :userId " +
                "AND m.matterCommunity.community.id = :communityId " +
                "AND m.status = :status "+
                "AND m.type = :types");
        query.setParameter("userId", userId);
        query.setParameter("communityId", communityId);
        query.setParameter("status", status);
        query.setParameter("types", sectionType);
        return query.getResultList();
    }

}
