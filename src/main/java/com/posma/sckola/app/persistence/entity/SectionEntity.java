package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Francis Ries on 18/04/2017.
 */
@Entity
@Table(name = "SECTION")
public class SectionEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "SECTION_SEQ", sequenceName = "S_ID_SECTION", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SECTION_SEQ")
    private Long id;

    @Column(name = "NAME", nullable=false)
    private  String name;

    @Column(name = "STATUS", nullable=false)
    private  StatusSection status;

    @ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JoinColumn(name = "COMMUNITY_ID", referencedColumnName = "ID", nullable=false)
    private  CommunityEntity community;


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "USER_SECTION", joinColumns = {
            @JoinColumn(name = "SECTION_ID", nullable = false, updatable = true) },
            inverseJoinColumns = { @JoinColumn(name = "USER_ID",
                    nullable = false, updatable = true) })
    private Set<UserEntity> studentList = new HashSet<UserEntity>();

    @OneToMany
    @JoinColumn(name = "SECTION_ID", referencedColumnName = "ID")
    private List<MatterCommunitySectionEntity>  matterCommunitySectionList;

    public SectionEntity() {

    }

    public SectionEntity(String name, CommunityEntity community, StatusSection status) {
        this.name = name;
        this.community = community;
        this.status = status;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StatusSection getStatus(){return status;}

    public void setStatus(StatusSection status){this.status = status;}

    public CommunityEntity getCommunity(){return community;}

    public void setCommunity(CommunityEntity community){this.community = community;}

    public Set<UserEntity> getStudentList() {
        return studentList;
    }

    public void setStudentList(Set<UserEntity> studentList) {
        this.studentList = studentList;
    }

    public boolean addStudentList(UserEntity user) {
        return studentList.add(user);
    }

    public boolean removeStudentList(UserEntity user) {
        return studentList.remove(user);
    }

    public List<MatterCommunitySectionEntity> getMatterCommunitySectionList() {
        return matterCommunitySectionList;
    }

    public void setMatterCommunitySectionList(List<MatterCommunitySectionEntity> matterCommunitySectionList) {
        this.matterCommunitySectionList = matterCommunitySectionList;
    }

    public boolean addMatterCommunitySectionList(MatterCommunitySectionEntity matterCommunitySection) {
        return matterCommunitySectionList.add(matterCommunitySection);
    }





}

