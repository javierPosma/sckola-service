package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.CurriculumEntity;

import java.util.List;

/**
 * Created by Francis Ries on 12/04/2017.
 */
public interface CurriculumDao {

    /**
     * description
     * @since 12/04/2017
     * @param id curriculum identifier
     * @return CurriculumEntity
     * @author PosmaGroup
     * @version 1.0
     */
    CurriculumEntity findOne(long id);

    /**
     * description
     * @since 12/04/2017
     * @return List<CurriculumEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<CurriculumEntity> findAll();

    /**
     * description
     * @since 12/04/2017
     * @param curriculum
     * @return CurriculumEntity
     * @author PosmaGroup
     * @version 1.0
     */
    CurriculumEntity create(CurriculumEntity curriculum);

    /**
     * description
     * @since 12/04/2017
     * @param curriculum
     * @return CurriculumEntity
     * @author PosmaGroup
     * @version 1.0
     */
    CurriculumEntity update(CurriculumEntity curriculum);

    /**
     * description
     * @since 12/04/2017
     * @param curriculum
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(CurriculumEntity curriculum);

    /**
     * description
     * @since 12/04/2017
     * @param id curriculum identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);
}
