package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.NetworksEntity;

import java.util.List;

/**
 * Created by Posma-dev on 30/08/2018.
 */
public interface NetworkDao {

    NetworksEntity findOne(long id);

    List<NetworksEntity> findAll();

    NetworksEntity create(NetworksEntity networks);

    NetworksEntity update(NetworksEntity networks);

    void delete(NetworksEntity networks);

    void deleteById(long id);

    List<NetworksEntity> networksByUser(Long userId);
}
