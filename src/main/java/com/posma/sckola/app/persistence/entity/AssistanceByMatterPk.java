package com.posma.sckola.app.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Posma-ricardo on 21/04/2017.
 */
@Embeddable
public class AssistanceByMatterPk implements Serializable {
    @Column(name = "USER_ID", nullable = false)
    private Long user;

    @Column(name = "MATTER_COMMUNITY_SECTION_ID", nullable = false)
    private Long matterCommunitySection;

    @Column(name = "DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;

    public AssistanceByMatterPk() {}

    public AssistanceByMatterPk(Long user, Long matterCommunitySection, Date date) {
        this.user = user;
        this.matterCommunitySection = matterCommunitySection;
        this.date = date;
    }

    public Long getUser(){
        return user;
    }

    public void setUser(Long user){
        this.user = user;
    }

    public Long getMatterCommunitySection(){
        return matterCommunitySection;
    }

    public void setMatterCommunitySection(Long matterCommunitySection){
        this.matterCommunitySection = matterCommunitySection;
    }

    public Date getDate(){
        return date;
    }

    public void setDate(Date date){
        this.date = date;
    }

    @Override
    public boolean equals(Object assistancePk){
        try {
            if (this.date.equals(((AssistanceByMatterPk)assistancePk).getDate()) &&
                    this.user.equals(((AssistanceByMatterPk)assistancePk).getUser()) &&
                    this.matterCommunitySection.equals(((AssistanceByMatterPk)assistancePk).getMatterCommunitySection()))
                return true;
        }catch(NullPointerException npe){

        }
        return false;
    }

    // equals, hashCode
}