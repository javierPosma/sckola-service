package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 12/04/2017.
 */
@Embeddable
public class Qualification implements Serializable {

    @Column(name = "VALUE", nullable=true, length = 80)
    private String value;

    @Column(name = "TEXT_VALUE", nullable=true, length = 80)
    private String text;

    @Column(name = "WEIGHT", nullable=true)
    private Integer weight;

    public Qualification() {

    }

    public Qualification(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public Qualification(String value, String text, Integer weight) {
        this.value = value;
        this.text = text;
        this.weight = weight;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

}
