package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 18/04/2017.
 */
@Entity
@Table(name = "MATTER_COMMUNITY",
        indexes = {
                @Index(columnList ="NAME",name = "IDX_NAME_MATTER_COMMUNITY"),
                @Index(columnList ="MATTER_ID",name = "IDX_TEACHER_MATTER_COMMUNITY"),
                @Index(columnList ="COMMUNITY_ID",name = "IDX_COMMUNITY_ID_MATTER_COMMUNITY")
        })
public class MatterCommunityEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "MATTER_COMMUNITY_SEQ", sequenceName = "S_ID_MATTER_COMMUNITY", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MATTER_COMMUNITY_SEQ")
    private Long id;

    @Column(name = "NAME", nullable=false)
    private String name;

    @Column(name = "CODE", nullable=true)
    private String code;

    @Column(name = "DESCRIPTION", nullable=true, length = 1024)
    private String description;

    @Column(name = "OBJECTIVE", nullable=true, length = 1024)
    private String objective;

    @Column(name = "ESPECIFIC_OBJECTIVE", nullable=true, columnDefinition = "text")
    private String especificObjective;

    @ManyToOne
    @JoinColumn(name = "MATTER_ID", referencedColumnName = "ID", nullable = true)
    private MatterEntity matter;

    @ManyToOne
    @JoinColumn(name = "COMMUNITY_ID", referencedColumnName = "ID", nullable = false)
    private CommunityEntity community;

/*
    @OneToOne
    @PrimaryKeyJoinColumn
    private EvaluationPlanEntity evaluationPlan;
*/

    public MatterCommunityEntity() {

    }

    public MatterCommunityEntity(String name, String description, MatterEntity matter, CommunityEntity community) {
        this.name = name;
        this.description = description;
        this.matter = matter;
        this.community = community;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getEspecificObjective() {
        return especificObjective;
    }

    public void setEspecificObjective(String especificObjective) {
        this.especificObjective = especificObjective;
    }

    public MatterEntity getMatter() {
        return matter;
    }

    public void setMatter(MatterEntity matter) {
        this.matter = matter;
    }

    public CommunityEntity getCommunity() {
        return community;
    }

    public void setCommunity(CommunityEntity community) {
        this.community = community;
    }

    /*
    public EvaluationPlanEntity getEvaluationPlan() {
        return evaluationPlan;
    }

    public void setEvaluationPlan(EvaluationPlanEntity evaluationPlan) {
        this.evaluationPlan = evaluationPlan;
    }
*/

}
