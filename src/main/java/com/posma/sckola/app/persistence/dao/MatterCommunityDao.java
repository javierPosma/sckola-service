package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.MatterCommunityEntity;

import java.util.List;

/**
 * Created by Francis Ries on 12/04/2017.
 */
public interface MatterCommunityDao {

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @return MatterCommunityEntity
     * @author PosmaGroup
     * @version 1.0
     */
    MatterCommunityEntity findOne(long id);

    /**
     * description
     * @since 12/04/2017
     * @return List<MatterCommunityEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<MatterCommunityEntity> findAll();

    /**
     * description
     * @since 12/04/2017
     * @return List<MatterCommunityEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<MatterCommunityEntity> findAllByFieldId(String fieldName, long id);

    /**
     * description
     * @since 12/04/2017
     * @param matterCommunity
     * @return MatterCommunityEntity
     * @author PosmaGroup
     * @version 1.0
     */
    MatterCommunityEntity create(MatterCommunityEntity matterCommunity);

    /**
     * description
     * @since 12/04/2017
     * @param matterCommunity
     * @return MatterCommunityEntity
     * @author PosmaGroup
     * @version 1.0
     */
    MatterCommunityEntity update(MatterCommunityEntity matterCommunity);

    /**
     * description
     * @since 12/04/2017
     * @param matterCommunity
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(MatterCommunityEntity matterCommunity);

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);


    /**
     * description
     * @since 20/04/2017
     * @return List<MatterCommunityEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<MatterCommunityEntity> findAllByMatterCommunityId(long matterId, long communityId);


    List<MatterCommunityEntity> findAllMatterByCommunityIdUserId(long communityId, long userId);


}
