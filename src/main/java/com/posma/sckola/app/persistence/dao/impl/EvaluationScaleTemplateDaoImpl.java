package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.EvaluationScaleTemplateDao;
import com.posma.sckola.app.persistence.entity.EvaluationScaleTemplateEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 27/04/2017.
 */

@Repository("EvaluationScaleTemplateDao")
public class EvaluationScaleTemplateDaoImpl extends AbstractDao<EvaluationScaleTemplateEntity> implements EvaluationScaleTemplateDao {

    public EvaluationScaleTemplateDaoImpl(){
        super ();
        setClazz(EvaluationScaleTemplateEntity.class);
    }


}
