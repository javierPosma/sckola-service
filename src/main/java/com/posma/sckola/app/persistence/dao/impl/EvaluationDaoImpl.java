package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.EvaluationDao;
import com.posma.sckola.app.persistence.entity.EvaluationEntity;
import com.posma.sckola.app.persistence.entity.StatusEvaluation;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Francis on 12/04/2017.
 */

@Repository("EvaluationDao")
public class EvaluationDaoImpl extends AbstractDao<EvaluationEntity> implements EvaluationDao {

    public EvaluationDaoImpl(){
        super ();
        setClazz(EvaluationEntity.class);
    }

    public List<EvaluationEntity> getAllEvaluationPlan(Long evaluationPlanId){
        Query query = this.getEntityManager().createQuery("SELECT m FROM EvaluationEntity as m WHERE m.evaluationPlan.id =:evaluationPlanId " +
                "AND ( m.status =:status OR m.status =:status2) ORDER BY m.date");
        query.setParameter("evaluationPlanId", evaluationPlanId);
        query.setParameter("status", StatusEvaluation.PENDING);
        query.setParameter("status2", StatusEvaluation.IN_PROGRESS);

        return query.getResultList();
    }

}
