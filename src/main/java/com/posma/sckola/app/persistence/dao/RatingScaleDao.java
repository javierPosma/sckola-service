package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.RatingScaleEntity;

import java.util.List;

/**
 * Created by Francis Ries on 12/04/2017.
 */
public interface RatingScaleDao {

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @return RatingScaleEntity
     * @author PosmaGroup
     * @version 1.0
     */
    RatingScaleEntity findOne(long id);

    /**
     * description
     * @since 12/04/2017
     * @return List<RatingScaleEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<RatingScaleEntity> findAll();

    /**
     * description
     * @since 12/04/2017
     * @param ratingScale
     * @return RatingScaleEntity
     * @author PosmaGroup
     * @version 1.0
     */
    RatingScaleEntity create(RatingScaleEntity ratingScale);

    /**
     * description
     * @since 12/04/2017
     * @param ratingScale
     * @return RatingScaleEntity
     * @author PosmaGroup
     * @version 1.0
     */
    RatingScaleEntity update(RatingScaleEntity ratingScale);

    /**
     * description
     * @since 12/04/2017
     * @param ratingScale
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(RatingScaleEntity ratingScale);

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);
}
