package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.QualificationUserDao;
import com.posma.sckola.app.persistence.entity.QualificationUserEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Francis on 12/04/2017.
 */

@Repository("QualificationUserDao")
public class QualificationUserDaoImpl extends AbstractDao<QualificationUserEntity> implements QualificationUserDao {

    public QualificationUserDaoImpl(){
        super ();
        setClazz(QualificationUserEntity.class);
    }

    public List<QualificationUserEntity> GetAllToStudent(Long evaluationPlanId, Long studentId){
        Query query = this.getEntityManager().createQuery("SELECT m FROM QualificationUserEntity as m WHERE m.user.id =:studentId and m.evaluation.evaluationPlan.id =:evaluationPlanId");
        query.setParameter("evaluationPlanId", evaluationPlanId);
        query.setParameter("studentId", studentId);

        return query.getResultList();
    }

    public QualificationUserEntity getToStudentEvaluation(Long evaluationId, Long studentId){
        Query query = this.getEntityManager().createQuery("SELECT m FROM QualificationUserEntity as m WHERE m.user.id =:studentId and m.evaluation.id =:evaluationId");
        query.setParameter("evaluationId", evaluationId);
        query.setParameter("studentId", studentId);

        List<QualificationUserEntity> qualificationUserEntityList = query.getResultList();

        return qualificationUserEntityList.size() > 0? qualificationUserEntityList.get(0):null;
    }

    public List<QualificationUserEntity> getAllToEvaluation(Long evaluationId){
        Query query = this.getEntityManager().createQuery("SELECT m FROM QualificationUserEntity as m WHERE m.evaluation.id =:evaluationId");
        query.setParameter("evaluationId", evaluationId);
        return query.getResultList();
    }

}
