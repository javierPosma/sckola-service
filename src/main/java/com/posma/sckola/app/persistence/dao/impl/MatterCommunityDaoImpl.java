package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.MatterCommunityDao;
import com.posma.sckola.app.persistence.entity.MatterCommunityEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 20/04/2017.
 */
@Repository("MatterCommunityDao")
public class MatterCommunityDaoImpl extends AbstractDao<MatterCommunityEntity> implements MatterCommunityDao {

    public MatterCommunityDaoImpl(){
        super();
        setClazz(MatterCommunityEntity.class);
    }


    /**
     * description
     * @since 20/04/2017
     * @return List<MatterCommunityEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    public List<MatterCommunityEntity> findAllByMatterCommunityId(long matterId, long communityId){
        Query query = this.getEntityManager().createQuery("SELECT m FROM MatterCommunityEntity as m WHERE  m.matter.id = :matterId " +
                "and m.community.id = :communityId");
            query.setParameter("matterId", matterId);
            query.setParameter("communityId", communityId);
        return query.getResultList();
    }

    public List<MatterCommunityEntity> findAllMatterByCommunityIdUserId(long communityId, long userId) {
        Query query = this.getEntityManager().createQuery("select mcs.matterCommunity.matter from MatterCommunitySectionEntity mcs where mcs.matterCommunity.community.id = :communityId and mcs.user.id = :userId");
        query.setParameter("communityId", communityId);
        query.setParameter("userId", userId);

        return query.getResultList();
    }

}
