package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.NetworkDao;
import com.posma.sckola.app.persistence.entity.NetworksEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Posma-dev on 30/08/2018.
 */
@Repository("NetworkDao")
public class NetworkDaoImpl extends AbstractDao<NetworksEntity> implements NetworkDao {

    public NetworkDaoImpl(){
        super();
        setClazz(NetworksEntity.class);
    }

    public List<NetworksEntity> networksByUser(Long userId){
        Query query = this.getEntityManager().createQuery("SELECT m FROM NetworksEntity as m WHERE  m.user.id = :userId ");
        query.setParameter("userId", userId);
        return query.getResultList();
    }
}
