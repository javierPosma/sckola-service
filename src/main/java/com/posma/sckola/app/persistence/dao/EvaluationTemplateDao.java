package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.EvaluationTemplateEntity;

import java.util.List;

/**
 * Created by Francis Ries on 27/04/2017.
 */
public interface EvaluationTemplateDao {

    /**
     * description
     * @since 27/04/2017
     * @param id evaluationTemplate identifier
     * @return EvaluationTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationTemplateEntity findOne(long id);

    /**
     * description
     * @since 27/04/2017
     * @return List<EvaluationTemplateEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationTemplateEntity> findAll();

    /**
     * description
     * @since 27/04/2017
     * @param evaluationTemplate
     * @return EvaluationTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationTemplateEntity create(EvaluationTemplateEntity evaluationTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param evaluationTemplate
     * @return EvaluationTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationTemplateEntity update(EvaluationTemplateEntity evaluationTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param evaluationTemplate
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(EvaluationTemplateEntity evaluationTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param id evaluationTemplate identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

}
