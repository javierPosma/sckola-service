package com.posma.sckola.app.persistence.dao;


import com.posma.sckola.app.persistence.entity.WizardEntity;

import java.util.List;

/**
 * Created by Francis on 29/05/2017.
 */
public interface WizardDao {

    /**
     * description
     * @since 27/03/2017
     * @param id Wizard identifier
     * @return WizardEntity
     * @author PosmaGroup
     * @version 1.0
     */
    WizardEntity findOne(long id);

    /**
     * description 
     * @since 27/03/2017
     * @return List<WizardEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<WizardEntity> findAll();

    /**
     * description 
     * @since 27/03/2017
     * @param fieldName column name
     * @param id Wizard identifier
     * @return List<WizardEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<WizardEntity> findAllByFieldId(String fieldName, long id);


    /**
     * description 
     * @since 27/03/2017
     * @param Wizard
     * @return WizardEntity
     * @author PosmaGroup
     * @version 1.0
     */
    WizardEntity create(WizardEntity Wizard);

    /**
     * description 
     * @since 27/03/2017
     * @param Wizard
     * @return WizardEntity
     * @author PosmaGroup
     * @version 1.0
     */
    WizardEntity update(WizardEntity Wizard);

    /**
     * description 
     * @since 27/03/2017
     * @param Wizard
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(WizardEntity Wizard);

    /**
     * description
     * @since 27/03/2017
     * @param id Wizard identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

}
