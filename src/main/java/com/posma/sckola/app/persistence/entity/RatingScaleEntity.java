package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 11/04/2017.
 */
@Entity
@Table(name = "RATING_SCALE")
public class RatingScaleEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "RATING_SCALE_SEQ", sequenceName = "S_ID_RATING_SCALE", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RATING_SCALE_SEQ")
    private Long id;

    @Column(name = "NAME", nullable=false, length = 50)
    private String name;

    @Column(name = "DESCRIPTION", nullable=true, length = 512)
    private String description;

    @OneToMany(mappedBy="ratingScale", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<RatingValueEntity> ratingValueList = new ArrayList<RatingValueEntity>();

    public RatingScaleEntity() {

    }

    public RatingScaleEntity(String name, String description) {
        this.name = name;
        this.description = description;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<RatingValueEntity> getRatingValueList() {
        return ratingValueList;
    }

    public void setRatingValueList(List<RatingValueEntity> ratingValueList) {
        this.ratingValueList = ratingValueList;
    }

    public boolean addRatingValueList(RatingValueEntity ratingValue) {
        return ratingValueList.add(ratingValue);
    }


}
