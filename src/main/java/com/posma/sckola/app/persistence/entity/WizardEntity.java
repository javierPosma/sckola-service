package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 29/05/2017.
 */
@Entity
@Table(name = "WIZARD",
        indexes = {
                @Index(columnList ="USER_ID",name = "IDX_USER_ID_WIZARD")
        })
public class WizardEntity implements Serializable {
/*
    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "WIZARD_SEQ", sequenceName = "S_ID_WIZARD", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "WIZARD_SEQ")
    private Long id;
*/
    @Id
    @OneToOne
    @JoinColumn(name = "USER_ID", referencedColumnName="ID", nullable=false)
    private UserEntity user;

    @Column(name = "USER_PROFILE", nullable= true)
    private long userProfile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMMUNITY_ID", nullable = true)
    private CommunityEntity community;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MATTER_COMMUNITY_SECTION_ID", nullable = true)
    private MatterCommunitySectionEntity matterCommunitySection;

    @ManyToOne
    @JoinColumn(name="EVALUATION_PLAN_ID", nullable = true)
    private EvaluationPlanEntity evaluationPlan;

    public WizardEntity() {}

    public WizardEntity(UserEntity user) {
        this.user = user;
    }



    /*
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
*/
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public CommunityEntity getCommunity() {
        return community;
    }

    public void setCommunity(CommunityEntity community) {
        this.community = community;
    }

    public MatterCommunitySectionEntity getMatterCommunitySection() {
        return matterCommunitySection;
    }

    public void setMatterCommunitySection(MatterCommunitySectionEntity matterCommunitySection) {
        this.matterCommunitySection = matterCommunitySection;
    }

    public EvaluationPlanEntity getEvaluationPlan() {
        return evaluationPlan;
    }

    public void setEvaluationPlan(EvaluationPlanEntity evaluationPlan) {
        this.evaluationPlan = evaluationPlan;
    }

    public long getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(long userProfile) {
        this.userProfile = userProfile;
    }

}
