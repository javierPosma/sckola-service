package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.AssistanceBF;
import com.posma.sckola.app.dto.AssistanceDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.util.SystemMessage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by Posma-ricardo on 21/04/2017.
 */
@RestController
@RequestMapping("/skola/assistance")
public class AssistanceController {

    static final Logger logger = Logger.getLogger(AssistanceController.class);

    @Autowired
    AssistanceBF assistanceBF;

    @Autowired
    SystemMessage systemMessage;

    /**
     * Servicio que permite crear una nueva asistencia en skola
     * @return Boolean true
     * @throws IOException
     */
    @RequestMapping(value = "/matter_community_section/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> assistanceCreate(@PathVariable("id") Long matterComunitySectionId, @RequestBody AssistanceDto assistanceDto) throws IOException {

        MessageDto messageDto;
        if(matterComunitySectionId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = assistanceBF.createAssistance(matterComunitySectionId, assistanceDto);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    /**
     * Servicio que permite actualizar una asistencia en skola
     * @return Boolean true
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> assistanceUpdate(@RequestBody AssistanceDto assistanceDto) throws IOException {

        MessageDto messageDto;
        if(assistanceDto == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }


        messageDto = assistanceBF.updateAssistance(assistanceDto);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    /**
     * Servicio que permite traer todas las assistencias de la fecha de una materia en skola
     * @return Boolean true
     * @throws IOException
     */
    @RequestMapping(value="/matter_community_section/{id}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> assistanceGetAllDate(@PathVariable("id") Long matterComunitySectionId,@RequestParam("date")String date) throws IOException {

        MessageDto messageDto;
        if(matterComunitySectionId == null && date == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }


        messageDto = assistanceBF.getAllDate(matterComunitySectionId,date);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);

    }

}
