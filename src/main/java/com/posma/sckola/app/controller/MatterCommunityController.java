package com.posma.sckola.app.controller;


import com.posma.sckola.app.business.MatterCommunityBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.SectionTypeDto;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


/**
 * Created by Francis on 20/04/2017.
 */
@RestController
@RequestMapping("/skola/matter_community")
public class MatterCommunityController {

    static final Logger logger = Logger.getLogger(MatterCommunityController.class);

    static final String MessageMatterApproveFail = "No fue posible procesar la solicitud";
    static final String MessageMatterApproveSuccess= "Solicitud procesada";

    @Autowired
    MatterCommunityBF matterCommunityBF;

    @Autowired
    Validation validation;
    
    @Autowired
    SystemMessage systemMessage;


    /**
     * Servicio consultar el detalle de una "materias plantillas"
     * @return List MatterDto
     * @throws IOException
     */
    @RequestMapping(value="/{matterCommunityId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matterGetById(@PathVariable("matterCommunityId") Long matterCommunityId) throws IOException {

        MessageDto messageDto;

        if(matterCommunityId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = matterCommunityBF.matterCommunityGetById(matterCommunityId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * listar materia existentes en la comunidad, donde un profesor(dicta) o como alumno(asiste)
     * sirve para asignar una evaluacion
     * @return List MatterDto
     * @throws IOException
     */

    @RequestMapping(value="/{matterCommunityId}/section/{sectionId}/user/{userId}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> matterCommunitySectionCreate(@PathVariable("matterCommunityId") Long matterCommunityId,
                                                          @PathVariable("sectionId") Long sectionId,
                                                          @PathVariable("userId") Long userId,
                                                          @RequestBody SectionTypeDto sectionType) throws IOException {

        MessageDto messageDto;

        if(matterCommunityId == null || sectionId == null || userId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = matterCommunityBF.matterCommunitySectionCreate(matterCommunityId, sectionId, userId, sectionType);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Servicio que permite consultar todas las materias de una comunidad
     * @return List MatterDto
     * @throws IOException
     */
    /*
    @RequestMapping(value="/community/{communityId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matterCommunityGetAllByCommunityId(@PathVariable("communityId") Long communityId) throws IOException {

        MessageDto messageDto;

        if(communityId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = matterCommunityBF.matterGetAllByCommunity(communityId);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }
*/
    
        /**
         * Funcion de manejo de excepciones pertenecientes a la clase Exception
         * @param exc excepcion interceptada
         * @return una entidad respuesta, con el HttpStatus y el mensaje del error
         */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        logger.error(exc.getMessage(), exc);
        exc.printStackTrace();
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
