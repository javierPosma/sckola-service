package com.posma.sckola.app.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@RestController
@RequestMapping("/skola/test")
public class TestController {

    @RequestMapping()
    public String helloWorld (){
        return "Hello, skola works!!";
    }
}
