package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.CurriculumBF;
import com.posma.sckola.app.dto.CurriculumDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.util.SystemMessage;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by Francis on 30/03/2017.
 */
@RestController
@RequestMapping("/skola/curriculum")
public class CurriculumController {

    static final Logger logger = Logger.getLogger(CurriculumController.class);

    static final String MessageLoginResourceNotFound = "Recurso solicitado no encontrado";
    static final String MessageLoginApproveFail = "No fue posible procesar la solicitud";
    static final String MessageLoginApproveSuccess= "Solicitud procesada";

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    CurriculumBF curriculumBF;


    /**
     * Servicio que permite registrar un nuevo curriculum (experiencia academica) en el sistema
     * @return Login ID
     * @throws IOException
     */
    @RequestMapping(value = "/user/{userId}",method = RequestMethod.POST)
    public ResponseEntity<?> addExperience(@PathVariable("userId") Long userId,
                                           @RequestBody CurriculumDto curriculumDto) throws IOException {

        if(curriculumDto != null && curriculumDto.getTitle() != null &&
                curriculumDto.getInstitute() != null &&
                curriculumDto.getDate() != null &&
                userId != null) {

            MessageDto messageDto = curriculumBF.curriculumCreate(curriculumDto, userId);
            if(messageDto != null && messageDto.getSuccess()){
                return ResponseEntity.ok(messageDto);
            }else{
                return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
            }
        }

        MessageDto messageDto = new MessageDto();
        messageDto.setSuccess(false);
        messageDto.setErrorCode("GET-000");
        messageDto.setMessage(systemMessage.getMessage("GET-000"));
        return new ResponseEntity<> (messageDto, HttpStatus.BAD_REQUEST);
    }


    /**
     * Servicio que permite actualizar una experiencia academica en el sistema
     * @return Login ID
     * @throws IOException
     */
    @RequestMapping(value = "/{curriculumId}",method = RequestMethod.PUT)
    public ResponseEntity<?> updateExperience(@PathVariable("curriculumId") Long curriculumId,
                                           @RequestBody CurriculumDto curriculumDto) throws IOException {

        if(curriculumDto != null && curriculumDto.getTitle() != null &&
                curriculumDto.getInstitute() != null &&
                curriculumDto.getDate() != null &&
                curriculumDto.getUserId() != null) {

            MessageDto messageDto = curriculumBF.curriculumUpdate(curriculumId, curriculumDto);
            if(messageDto != null && messageDto.getSuccess()){
                return ResponseEntity.ok(messageDto);
            }else{
                return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
            }
        }

        MessageDto messageDto = new MessageDto();
        messageDto.setSuccess(false);
        messageDto.setErrorCode("GET-000");
        messageDto.setMessage(systemMessage.getMessage("GET-000"));
        return new ResponseEntity<> (messageDto, HttpStatus.BAD_REQUEST);
    }


    /**
     * Funcion de manejo de excepciones pertenecientes a la clase Exception
     * @param exc excepcion interceptada
     * @return una entidad respuesta, con el HttpStatus y el mensaje del error
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        logger.error(exc.getMessage(), exc);
        exc.printStackTrace();
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }


}
