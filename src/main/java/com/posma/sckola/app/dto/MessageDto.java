package com.posma.sckola.app.dto;

/**
 * Created by Francis on 27/03/2017.
 */
public class MessageDto {

    /*
     *  Identificator the original object
     */
    private Long idRequest;

    /*
     *  Identificator to Success transaction
     */
    private Boolean success = true;

    /*
     *  Error Code from Message to NorthBound
     */
    private String errorCode = "";

    /*
     *  Message to NorthBound
     */
    private String message = "";


    /*
     *  Object response to NorthBound example: projectDto, RoleDto, InvitationDto, etc...
     */
    private Object response;

    public MessageDto(){

    }

    public void setIdRequest(Long idRequest){
        this.idRequest = idRequest;
    }

    public Long getIdRequest(){
        return idRequest;
    }

    public void setSuccess(Boolean success){
        this.success = success;
    }

    public Boolean getSuccess(){
        return success;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public void setErrorCode(String errorCode){
        this.errorCode = errorCode;
    }

    public String getErrorCode(){
        return errorCode;
    }

    public void setResponse(Object response){
        this.response = response;
    }

    public Object getResponse(){
        return response;
    }


}
