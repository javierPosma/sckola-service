package com.posma.sckola.app.dto;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 25/04/2017.
 */
public class EvaluationScaleDto implements Serializable {

    private Long id;

    private String name;

    private String description;

    private List<EvaluationValueDto> evaluationValueList = new ArrayList<EvaluationValueDto>();

    public EvaluationScaleDto() {

    }

    public EvaluationScaleDto(String name, String description) {
        this.name = name;
        this.description = description;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<EvaluationValueDto> getEvaluationValueList() {
        return evaluationValueList;
    }

    public void setEvaluationValueList(List<EvaluationValueDto> ratingValueList) {
        this.evaluationValueList = ratingValueList;
    }

    public boolean addEvaluationValueList(EvaluationValueDto evaluationValue) {
        return evaluationValueList.add(evaluationValue);
    }


}
