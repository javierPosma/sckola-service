package com.posma.sckola.app.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Francis Ries on 19/04/2017.
 *
 * Aun no en uso
 */
public class UserCurriculumDto implements Serializable {

    private Long id;

    private String title;

    private CommunityDto community;

    private Date date;

    public UserCurriculumDto() {

    }

    public UserCurriculumDto(String title, CommunityDto community, Date date) {
        this.title = title;
        this.community = community;
        this.date = date;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CommunityDto getCommunity() {
        return community;
    }

    public void setCommunity(CommunityDto community) {
        this.community = community;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'title':'" + title + "',\n" +
                " 'community':" + community.toString() + ",\n" +
                " 'date':'" + date + "'\n" +
                "}";
    }

}
