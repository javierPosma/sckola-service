package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Francis Ries on 20/04/2017.
 */

public class MatterCommunityDto implements Serializable {

    private Long id;

    private String name;

    private String code;

    private Long matterId;

    private Long communityId;

    private String communityName;

    private String description;

    private String objective;

    private String especificObjective;



    public MatterCommunityDto() {

    }

    public MatterCommunityDto(String name, String description) {
        this.name = name;
        this.description = description;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getMatterId() {
        return matterId;
    }

    public void setMatterId(Long matterId) {
        this.matterId = matterId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getEspecificObjective() {
        return especificObjective;
    }

    public void setEspecificObjective(String especificObjective) {
        this.especificObjective = especificObjective;
    }

    public void setCommunityName(String communityName){
        this.communityName = communityName;
    }

    public String getCommunityName(){
        return communityName;
    }
}
