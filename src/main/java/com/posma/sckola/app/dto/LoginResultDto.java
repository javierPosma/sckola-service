package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Francis Ries on 27/03/2017.
 */
public class LoginResultDto implements Serializable {


    private long userId;

    private UserDto userDetail;

    private String token;

    public LoginResultDto() { }


    public long getUserId(){
        return userId;
    }

    public void setUserId(long userId){
        this.userId = userId;
    }

    public String getToken(){
        return token;
    }

    public void setToken(String token){
        this.token = token;
    }

    public UserDto getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDto userDetail) {
        this.userDetail = userDetail;
    }
    

}
