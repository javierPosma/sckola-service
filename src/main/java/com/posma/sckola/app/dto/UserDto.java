package com.posma.sckola.app.dto;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 27/03/2017.
 */
public class UserDto implements Serializable {

    private Long id;

    private String identification;

    private String firstName;

    private String secondName;

    private String lastName;

    private String secondLastName;

    private String mail;

    private String status;

    private String password;

    // format dd-MM-YYYY
    private String birthdate;

    //MASCULINO o FEMENINO
    private String gender;

    private String foto;

    private String mailRepresentative;

    //private List<NetworksDto> networks = new ArrayList<NetworksDto>();

    private List<RoleDto> roleList = new ArrayList<RoleDto>();

    private List<CurriculumDto> curriculum = new ArrayList<CurriculumDto>();


    public UserDto() { }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<RoleDto> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<RoleDto> roleList) {
        this.roleList = roleList;
    }

    public boolean addRoleList(RoleDto user) {
        return roleList.add(user);
    }

    public String getFoto(){return foto;}

    public void setFoto(String foto){this.foto = foto;}

    public List<CurriculumDto> getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(List<CurriculumDto> curriculum) {
        this.curriculum = curriculum;
    }

    public boolean addCurriculum(CurriculumDto experience) {
        return curriculum.add(experience);
    }

    public String getMailRepresentative(){return mailRepresentative;}

    public void setMailRepresentative(String mailRepresentative){this.mailRepresentative = mailRepresentative;}

    //public List<NetworksDto> getNetworks(){ return networks;}
//
    //public void setNetworks(List<NetworksDto> networks){ this.networks = networks;}
//
    //public boolean addNetworksList(NetworksDto networksDto){
    //    return networks.add(networksDto);
    //}

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'identification':'" + identification + "',\n" +
                " 'firstName':'" + firstName + "',\n" +
                " 'secondName':'" + secondName + "',\n" +
                " 'lastName':'" + lastName + "',\n" +
                " 'secondLastName':'" + secondLastName + "',\n" +
                " 'mail':'" + mail + "',\n" +
                " 'status':'" + status + "',\n" +
                " 'password':'**************'\n" +
                " 'gender':'" + gender + "',\n" +
                " 'birthdate':'" + birthdate + "',\n" +
               // " 'curriculum':" + (curriculum.size()>0?curriculum.toString():"{}") + " \n" +
                "}";
    }


}
