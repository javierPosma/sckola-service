package com.posma.sckola.app.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
public class EvaluationDto implements Serializable {

    private Long id;

    private String objective;

    private Integer weight;

    private EvaluationToolDto evaluationTool;

    private EvaluationScaleDto evaluationScale;

    private String date;

    private String status;

    private Long evaluationPlanId;

    private List<QualificationUserDto> qualificationUserList;


    public EvaluationDto(){}

    public Long getId(){return id;}

    public void setId(Long id){this.id = id;}

    public Long getEvaluationPlanId(){return evaluationPlanId;}

    public void setEvaluationPlanId(Long evaluationPlanId){this.evaluationPlanId = evaluationPlanId;}

    public String getObjective(){return objective;}

    public void setObjective(String objective){this.objective = objective;}

    public Integer getWeight(){return weight;}

    public void setWeight(Integer weight){this.weight = weight;}

    public EvaluationToolDto getEvaluationTool(){return evaluationTool;}

    public void setEvaluationTool(EvaluationToolDto evaluationTool){this.evaluationTool = evaluationTool;}

    public EvaluationScaleDto getEvaluationScale(){return evaluationScale;}

    public void setEvaluationScale(EvaluationScaleDto evaluationScale){this.evaluationScale = evaluationScale;}

    public String getDate(){return date;}

    public void setDate(String date){this.date = date;}

    public String getStatus(){return status;}

    public void setStatus(String status){this.status = status;}

    public List<QualificationUserDto> getQualificationUserList(){return qualificationUserList;}

    public void setQualificationUserList(List<QualificationUserDto> qualificationUserList){this.qualificationUserList = qualificationUserList;}
}
