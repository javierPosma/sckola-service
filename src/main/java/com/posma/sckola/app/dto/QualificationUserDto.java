package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
public class QualificationUserDto implements Serializable {

    private Long id;

    private UserDto user;

    private String nameEvaluation;

    private String dateEvaluation;

    private MatterDto matter;

    private QualificationDto qualification;

    public QualificationUserDto(){}

    public Long getId(){return id;}

    public void setId(Long id){this.id = id;}

    public UserDto getUser(){return user;}

    public void setUser(UserDto user){this.user = user;}

    public String getNameEvaluation(){return nameEvaluation;}

    public void setNameEvaluation(String nameEvaluation){this.nameEvaluation = nameEvaluation;}

    public String getDateEvaluation(){return dateEvaluation;}

    public void setDateEvaluation(String dateEvaluation){this.dateEvaluation = dateEvaluation;}

    public MatterDto getMatter(){return matter;}

    public void setMatter(MatterDto matter){this.matter = matter;}

    public QualificationDto getQualification(){return qualification;}

    public void setQualification(QualificationDto qualification){this.qualification = qualification;}

}
