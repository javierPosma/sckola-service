package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Francis Ries on 18/04/2017.
 */
public class RequestAssociateDto implements Serializable {

    private Boolean associate;

    public RequestAssociateDto(){}

    public RequestAssociateDto(boolean associate){
        this.associate = associate;
    }


    public Boolean getAssociate(){return associate;}

    public void setAssociate(Boolean associate){this.associate = associate;}

}
