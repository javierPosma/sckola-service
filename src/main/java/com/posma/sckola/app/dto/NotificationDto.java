package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Francis Ries on 16/02/2017.
 */
public class NotificationDto implements Serializable {

    private String to;

    private String title;

    private String text;

    public NotificationDto() { }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString(){
        return "{ \n" +
                " 'to':" + to + ",\n" +
                " 'title':" + title + ",\n" +
                " 'text':'" + text + "',\n" +
                "}";
    }
}
