package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Francis Ries on 25/04/2017.
 */
public class EvaluationValueDto implements Serializable {

    private Long id;

    private String value;

    private String text;

    private Integer weight;


    public EvaluationValueDto() {

    }

    public EvaluationValueDto(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public EvaluationValueDto(String value, String text, Integer weight) {
        this.value = value;
        this.text = text;
        this.weight = weight;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

}
