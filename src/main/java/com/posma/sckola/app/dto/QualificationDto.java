package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
public class QualificationDto implements Serializable {

    private String value;

    private String text;

    private Integer weight;

    public QualificationDto(){}

    public QualificationDto(String value, String text, Integer weight){
        this.value = value;
        this.text = text;
        this.weight = weight;
    }

    public String getValue(){return value;}

    public void setValue(String value){this.value = value;}

    public String getText(){return text;}

    public void setText(String text){this.text = text;}

    public Integer getWeight(){return weight;}

    public void setWeight(Integer weight){this.weight = weight;}
}
