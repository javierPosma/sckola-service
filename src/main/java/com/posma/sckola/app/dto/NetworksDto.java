package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Posma-dev on 30/08/2018.
 */
public class NetworksDto implements Serializable {

    private Long id;

    private String password;

    private String network;

    public NetworksDto(){ }

    public Long getId(){ return id; }

    public void setId(Long id){ this.id = id;}

    public String getPassword() { return password;}

    public void setPassword(String password){ this.password = password;}

    public String getNetwork() { return network;}

    public void setNetwork(String network){ this.network = network;}

}
