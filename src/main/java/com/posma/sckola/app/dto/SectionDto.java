package com.posma.sckola.app.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Francis Ries on 21/04/2017.
 */

public class SectionDto implements Serializable {

    private Long id;

    private  String name;

    private  Long comunityId;

    private List<UserDto> studentList;

    private Long countStudent;

    //private List<MatterCommunitySectionDto> matterCommunitySectionList = new ArrayList<MatterCommunitySectionDto>();

    public SectionDto() {

    }

    public SectionDto(Long id, String name, Long comunityId) {
        this.id = id;
        this.name = name;
        this.comunityId = comunityId;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getComunityId() {
        return comunityId;
    }

    public void setComunityId(Long comunityId) {
        this.comunityId = comunityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserDto> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<UserDto> studentList) {
        this.studentList = studentList;
    }

    public boolean addStudentList(UserDto user) {
        return studentList.add(user);
    }

    public Long getCountStudent(){ return countStudent;}

    public void setCountStudent(Long countStudent){this.countStudent = countStudent;}

    /*
    public List<MatterCommunitySectionDto> getMatterCommunitySectionList() {
        return matterCommunitySectionList;
    }

    public void setMatterCommunitySectionList(List<MatterCommunitySectionDto> matterCommunitySectionList) {
        this.matterCommunitySectionList = matterCommunitySectionList;
    }

    public boolean addMatterCommunitySectionList(MatterCommunitySectionDto matterCommunitySection) {
        return matterCommunitySectionList.add(matterCommunitySection);
    }

*/
}

