package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Francis Ries on 20/04/2017.
 */
public class CurriculumDto implements Serializable {

    private Long id;

    private Long userId;

    private String title;

    private CommunityDto community;

    private String date;

    private String institute;

    public CurriculumDto() {

    }

    public CurriculumDto(Long userId, String title, CommunityDto community, String date) {
        this.userId = userId;
        this.title = title;
        this.community = community;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public CommunityDto getCommunity() {
        return community;
    }

    public void setCommunity(CommunityDto community) {
        this.community = community;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'userId':" + userId + ",\n" +
                " 'title':'" + title + "',\n" +
                " 'institute':'" + institute + "',\n" +
                " 'community':" + community != null?community.toString():"" + ",\n" +
                " 'date':'" + date + "'\n" +
                "}";
    }

}
