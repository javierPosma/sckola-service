package com.posma.sckola.app.dto;

import com.posma.sckola.app.persistence.entity.NotificationType;

import java.util.Date;

/**
 * Created by Posma-dev on 17/08/2017.
 */
public class DashboardNotificationDto {

    private long id;
    private String from;
    private String text;
    private String image;
    private String username;
    private CommunityDto community;
    private MatterDto matter;
    private SectionDto section;
    private EvaluationDto evaluation;
    private boolean active;
    private NotificationType type;
    private String date;

    public DashboardNotificationDto() {
    }

    public DashboardNotificationDto(long id, String from, String text, String image, String username, CommunityDto community, MatterDto matter, NotificationType type) {
        this.id = id;
        this.from = from;
        this.text = text;
        this.image = image;
        this.username = username;
        this.community = community;
        this.matter = matter;
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public CommunityDto getCommunity() {
        return community;
    }

    public void setCommunity(CommunityDto community) {
        this.community = community;
    }

    public MatterDto getMatter() {
        return matter;
    }

    public void setMatter(MatterDto matter) {
        this.matter = matter;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public SectionDto getSection() {
        return section;
    }

    public void setSection(SectionDto section) {
        this.section = section;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public EvaluationDto getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(EvaluationDto evaluation) {
        this.evaluation = evaluation;
    }
}
