package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.DashboardNotificationDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.NotificationDto;
import com.posma.sckola.app.persistence.entity.*;
import org.springframework.messaging.Message;

import java.util.List;

/**
 * Created by Francis on 16/02/2017.
 */
public interface NotificationBF {

    /**
     * Service that create new notification
     * @since 01/08/2017
     * @param notificationDto object NotificationDto
     * @return identifier notification
     * @author Oswaldo Zárate
     * @version 1.0
     */
    Boolean sendNotification(NotificationDto notificationDto);

    /**
     * Service that checks for new notifications in the system.
     * @since 01/08/2017
     * @author Oswaldo Zárate
     * @version 1.0
     */
    void newNotifications();

    /**
     * Service that gets all notifications to send
     * @since 02/08/2017
     * @return List of DashboardNotificationDto
     * @author Oswaldo Zárate
     * @version 1.0
     */
    List<DashboardNotificationDto> getNotifications();

    /**
     * Service that gets notification for a user in a specific community
     * @since 16/02/2017
     * @param userMail Email of the user
     * @param id Id of the community
     * @return MessageDto with all the notifications for the user
     * @author Oswaldo Zárate
     * @version 1.0
     */
    MessageDto getActiveNotificationsByUser (String userMail, Long id);


    MessageDto getHistoryNotificationByUser(String userMail, Long idCommunity);

    /**
     * Service that sets a notification status to SENDED after it was sended to the client.
     * @since 16/02/2017
     * @param notification Notification to Update
     * @return boolean
     * @author Oswaldo Zárate
     * @version 1.0
     */
    boolean updateNotificationStatus(DashboardNotificationDto notification);

    /**
     * Service that sets a notification to inactive.
     * @since 16/02/2017
     * @param id Id of the notification
     * @return identifier notification
     * @author Oswaldo Zárate
     * @version 1.0
     */
    MessageDto removeActiveNotification(Long id);

    MessageDto getHistoryNotificationByInfo(String userMail);

    NotificationEntity createNotification(UserEntity user, MatterCommunitySectionEntity matterCommunitySection, NotificationType type, EvaluationEntity evaluation);
}


