package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.CommunityDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.RequestAssociateDto;

/**
 * Created by francis Ries on 26/05/2017.
 */
public interface CommunityUserBF {


    MessageDto getById(Long communityUserId);

    MessageDto updateCommunity(Long communityUserId, CommunityDto communityDto);

    /**
     * Servicio que permite consultar todas las comunidades donde participa un usuarioo del sistema segun el role (TEACHER/STUDENT)
     * @param userId
     * @param roleId
     * @return MessageDto
     */
    MessageDto getByUserIdRoleId(Long userId, Long roleId);


    /**
     * Servicio que permite AsociarComunidad y Desasociar un usuario del sistema con algun role (TEACHER/STUDENT) a una comunidad
     * @param communityUserId
     * @param userId
     * @param roleId
     * @return MessageDto
     */
    MessageDto setCommunityIdUserIdRoleId(Long communityUserId, Long userId, Long roleId, RequestAssociateDto requestAsociateDto);


}
