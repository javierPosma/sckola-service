package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.AssistanceBF;
import com.posma.sckola.app.dto.AssistanceDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.UserAssistDto;
import com.posma.sckola.app.persistence.dao.AssistanceByMatterDao;
import com.posma.sckola.app.persistence.dao.AssistanceIntegralDao;
import com.posma.sckola.app.persistence.dao.MatterCommunitySectionDao;
import com.posma.sckola.app.persistence.dao.NotificationDao;
import com.posma.sckola.app.persistence.dao.SectionDao;
import com.posma.sckola.app.persistence.entity.AssistanceByMatterEntity;
import com.posma.sckola.app.persistence.entity.AssistanceByMatterPk;
import com.posma.sckola.app.persistence.entity.AssistanceIntegralEntity;
import com.posma.sckola.app.persistence.entity.AssistanceIntegralPk;
import com.posma.sckola.app.persistence.entity.MatterCommunitySectionEntity;
import com.posma.sckola.app.persistence.entity.NotificationEntity;
import com.posma.sckola.app.persistence.entity.SectionEntity;
import com.posma.sckola.app.persistence.entity.UserEntity;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Posma-ricardo on 21/04/2017.
 */
@Service
public class AssistanceBFImpl implements AssistanceBF{

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    AssistanceByMatterDao assistanceByMatterDao;

    @Autowired
    AssistanceIntegralDao assistanceIntegralDao;

    @Autowired
    SectionDao sectionDao;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    Validation validation;

    @Override
    @Transactional
    public MessageDto createAssistance(Long matterComunitySectionId, AssistanceDto assistanceDto){

        MessageDto messageDto = new MessageDto();

        try{
            MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(matterComunitySectionId);
            if(matterCommunitySectionEntity == null || matterCommunitySectionEntity.getSection() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            SectionEntity sectionEntity = matterCommunitySectionEntity.getSection();
            if(matterCommunitySectionEntity.getType().name().equals("NON_INTEGRAL")){
                for(UserAssistDto userAssistDto:assistanceDto.getUserAssistList()){
                    AssistanceByMatterEntity assistanceByMatterEntity =new AssistanceByMatterEntity();
                    assistanceByMatterEntity.setId(new AssistanceByMatterPk(userAssistDto.getUserId(),matterComunitySectionId,validation.stringToDate(assistanceDto.getDate())));
                    assistanceByMatterEntity.setAttended(userAssistDto.getAssistance());
                    assistanceByMatterDao.create(assistanceByMatterEntity);
                }

                NotificationEntity notification = notificationDao.findByDateAndSectionAssistanceNotification(validation.stringToDate(assistanceDto.getDate()),matterCommunitySectionEntity);
                notification.setActive(false);
                notificationDao.update(notification);

                messageDto.setSuccess(true);
                return messageDto;
            }else{
                for(UserAssistDto userAssistDto: assistanceDto.getUserAssistList()){
                    AssistanceIntegralEntity assistanceIntegralEntity = new AssistanceIntegralEntity();
                    assistanceIntegralEntity.setId(new AssistanceIntegralPk(userAssistDto.getUserId(), sectionEntity.getId(), validation.stringToDate(assistanceDto.getDate())));
                    assistanceIntegralEntity.setAttended(userAssistDto.getAssistance());
                    assistanceIntegralDao.create(assistanceIntegralEntity);
                }
                /**
                 * Desactivar notificación.
                 * */
                NotificationEntity notification = notificationDao.findByDateAndSectionAssistanceNotification(validation.stringToDate(assistanceDto.getDate()),matterCommunitySectionEntity);
                notification.setActive(false);
                notificationDao.update(notification);

                messageDto.setSuccess(true);
                messageDto.setMessage(systemMessage.getMessage("SK-000")+ " - Creation Exitosa");
                return messageDto;
            }
        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Creation not exists");
            return messageDto;
        }


    }

    @Override
    @Transactional
    public MessageDto updateAssistance(AssistanceDto assistanceDto){

        MessageDto messageDto = new MessageDto();

        try {
            MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(assistanceDto.getMatterCommunitySectionId());

            if(matterCommunitySectionEntity == null || matterCommunitySectionEntity.getSection() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            SectionEntity sectionEntity = matterCommunitySectionEntity.getSection();

            if (matterCommunitySectionEntity.getType().name().equals("NON_INTEGRAL")) {
                for (UserAssistDto userAssistDto : assistanceDto.getUserAssistList()) {
                    AssistanceByMatterEntity assistanceByMatterEntity = assistanceByMatterDao.findOneAssistance(new AssistanceByMatterPk(userAssistDto.getUserId(), matterCommunitySectionEntity.getId(), validation.stringToDate(assistanceDto.getDate())));
                    assistanceByMatterEntity.setAttended(userAssistDto.getAssistance());
                    assistanceByMatterDao.update(assistanceByMatterEntity);
                }
                return messageDto;
            } else {
                for (UserAssistDto userAssistDto : assistanceDto.getUserAssistList()) {
                    AssistanceIntegralEntity assistanceIntegralEntity = assistanceIntegralDao.findOneAssistance(new AssistanceIntegralPk(userAssistDto.getUserId(), sectionEntity.getId(), validation.stringToDate(assistanceDto.getDate())));
                    assistanceIntegralEntity.setAttended(userAssistDto.getAssistance());
                    assistanceIntegralDao.update(assistanceIntegralEntity);
                }
                return messageDto;
            }
        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Upgrade not exit");
            return messageDto;
        }
    }

    @Override
    @Transactional
    public MessageDto getAllDate(Long matterComunitySectionId,String date){

        MessageDto messageDto = new MessageDto();
        try {
            MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(matterComunitySectionId);

            if(matterCommunitySectionEntity == null || matterCommunitySectionEntity.getSection() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            SectionEntity sectionEntity = matterCommunitySectionEntity.getSection();
            AssistanceDto assistanceDto = new AssistanceDto();
            if (matterCommunitySectionEntity.getType().name().equals("NON_INTEGRAL")) {
                AssistanceByMatterPk assistanceByMatterPk = new AssistanceByMatterPk();
                assistanceByMatterPk.setDate(validation.stringToDate(date));
                assistanceByMatterPk.setMatterCommunitySection(matterComunitySectionId);
                List<AssistanceByMatterEntity> assistanceByMatterEntityList = assistanceByMatterDao.findAllForSectionForDate(assistanceByMatterPk);
                List<UserAssistDto> userAssistDtoList = new ArrayList<UserAssistDto>();
                for (AssistanceByMatterEntity assistanceByMatterEntity : assistanceByMatterEntityList) {
                    UserAssistDto userAssistDto = new UserAssistDto();
                    userAssistDto.setUserId(assistanceByMatterEntity.getId().getUser());
                    userAssistDto.setAssistance(assistanceByMatterEntity.getAttended());
                    for(UserEntity userEntity: sectionEntity.getStudentList()){
                        if(userEntity.getId() == assistanceByMatterEntity.getId().getUser()){
                            userAssistDto.setFirstName(userEntity.getFirstName());
                            userAssistDto.setLastName(userEntity.getLastName());
                        }
                    }
                    userAssistDtoList.add(userAssistDto);
                }
                assistanceDto.setUserAssistList(userAssistDtoList);
                assistanceDto.setDate(date);
                assistanceDto.setMatterCommunitySectionId(matterComunitySectionId);
                messageDto.setResponse(assistanceDto);
                return messageDto;
            } else {
                AssistanceIntegralPk assistanceIntegralPk = new AssistanceIntegralPk();
                assistanceIntegralPk.setDate(validation.stringToDate(date));
                assistanceIntegralPk.setSectionId(sectionEntity.getId());
                List<AssistanceIntegralEntity> assistanceIntegralEntityList = assistanceIntegralDao.findAllForSectionForDate(assistanceIntegralPk);
                List<UserAssistDto> userAssistDtoList = new ArrayList<UserAssistDto>();
                for (AssistanceIntegralEntity assistanceIntegralEntity : assistanceIntegralEntityList) {
                    UserAssistDto userAssistDto = new UserAssistDto();
                    userAssistDto.setUserId(assistanceIntegralEntity.getId().getUser());
                    userAssistDto.setAssistance(assistanceIntegralEntity.getAttended());
                    for(UserEntity userEntity: sectionEntity.getStudentList()){
                        if(userEntity.getId() == assistanceIntegralEntity.getId().getUser()){
                            userAssistDto.setFirstName(userEntity.getFirstName());
                            userAssistDto.setLastName(userEntity.getLastName());
                        }
                    }
                    userAssistDtoList.add(userAssistDto);
                }
                assistanceDto.setUserAssistList(userAssistDtoList);
                assistanceDto.setDate(date);
                assistanceDto.setMatterCommunitySectionId(matterComunitySectionId);
                messageDto.setResponse(assistanceDto);
                return messageDto;
            }
        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Search not exit");
            return messageDto;
        }
    }
}
