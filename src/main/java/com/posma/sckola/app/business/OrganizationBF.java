package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.OrganizationDto;
import java.util.List;

/**
 * Created by Francis on 27/03/2017.
 */
public interface OrganizationBF {

    /**
     * carga los organizations existentes en el sistema
     * @since 27/03/2017
     * @return oraganization list
     * @author FrancisRies
     * @version 1.0
     */
    List<OrganizationDto> getAll();


}


