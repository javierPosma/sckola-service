package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;


/**
 * Created by Francis Ries on 27/03/2017.
 */
public interface MatterBF {


    /**
     * Service query all Matters
     * @since 20/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto matterGetAll();


    /**
     * Service that copy a Matter to a Community
     * @since 20/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto matterAssociate(Long matterId, Long communityId, boolean associate);


    /**
     * Service query all Matters from a community
     * @since 20/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto matterGetAllByCommunity(Long communityId);



    /**
     * Service get detall from Matters from community
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto matterGetByIdCommunityId(Long matterId, Long communityId);


    /**
     * Service get detall from Matters
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto matterGetById(Long matterId);

    /**
     * list matter from community, by witch a teacher(teaches) or a student(attends)
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto matterCommunityGetAllByUserRole(Long communityId, Long roleId, Long userId);


    /**
     * list matter from community, by witch a teacher(teaches) or a student(attends)
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto matterCommunityGetAllByUserRoleWithoutEvaluationPlan(Long communityId, Long roleId, Long userId);

    /**
     * list matter from community, by witch a teacher(teaches) or a student(attends) and type(Integral or not Integral)
     * @since 10/05/2017
     * @author RicardoBalza
     * @version 1.0
     */
    MessageDto matterCommunityGetAllByUserRoleByType(Long communityId, Long roleId, Long userId, String type);
    /**
     * Listar todas las materias impartidas por un profesor
     * @since 17/05/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto matterCommunityGetAllByTeacher(Long roleId, Long userId);




    /**
     * Service to create matter
     * @since 20/04/2017
     * @param matterDto
     * @param userId
     * @author FrancisRies
     * @version 1.0
     */
    //MessageDto matterCreate(MatterDto matterDto, Long userId);


    /**
     * Service to update data from matter
     * @since 20/04/2017
     * @param matterId matter identifier
     * @param matterDto matter dto with all params to update
     * @author FrancisRies
     * @version 1.0
     */
    //MessageDto matterUpdate(Long matterId, MatterDto matterDto);

}


