package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.UserDto;

/**
 * Created by Posma-dev on 07/09/2018.
 */
public interface NetworksBF {

    MessageDto GetAllNetworksUser(UserDto userDto);
}
