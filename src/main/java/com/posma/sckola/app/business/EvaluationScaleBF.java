package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;


/**
 * Created by Francis Ries on 25/04/2017.
 */
public interface EvaluationScaleBF {

    /**
     * Service query all Evaluation tools
     * @since 25/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto getAll();

}


