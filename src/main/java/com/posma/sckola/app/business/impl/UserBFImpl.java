package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.CommunityBF;
import com.posma.sckola.app.business.CommunityUserBF;
import com.posma.sckola.app.business.NotificationBF;
import com.posma.sckola.app.business.UserBF;
import com.posma.sckola.app.business.WizardBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.NotificationDto;
import com.posma.sckola.app.dto.NotificationHistoryDto;
import com.posma.sckola.app.dto.RequestAssociateDto;
import com.posma.sckola.app.dto.UserDto;
import com.posma.sckola.app.persistence.dao.NetworkDao;
import com.posma.sckola.app.persistence.dao.NotificationDao;
import com.posma.sckola.app.persistence.dao.PhotoDirectionDao;
import com.posma.sckola.app.persistence.dao.RoleDao;
import com.posma.sckola.app.persistence.dao.UserDao;
import com.posma.sckola.app.persistence.dao.UserRoleCommunityDao;
import com.posma.sckola.app.persistence.dao.ValidationAccountDao;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.Encrypt;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.NotificationMessage;
import com.posma.sckola.app.util.ServiceProperties;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@Service
public class UserBFImpl implements UserBF {

    @Autowired
    UserDao userDao;

    @Autowired
    RoleDao roleDao;

    @Autowired
    PhotoDirectionDao photoDirectionDao;

    @Autowired
    Encrypt encrypt;

    @Autowired
    ValidationAccountDao validationAccountDao;

    @Autowired
    NotificationBF notificationBF;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    Validation validation;

    @Autowired
    CommunityBF communityBF;

    @Autowired
    CommunityUserBF communityUserBF;

    @Autowired
    UserRoleCommunityDao userRoleCommunityDao;

    @Autowired
    ServiceProperties serviceProperties;

    @Autowired
    WizardBF wizardBf;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    NetworkDao networkDao;

    @Autowired
    NotificationBF notificationBf;

    @Autowired
    NotificationMessage notificationMessage;

    private final static int day = 1;

    /**
     * Service to create user
     *
     * @author FrancisRies
     * @version 1.0
     * @since 27/03/2017
     */
    @Override
    @Transactional
    public MessageDto userCreate(UserDto userDto) {

        MessageDto messageDto = new MessageDto();
        messageDto.setMessage("User created");

        // valores para plantilla

        final String textKeyLogo = "<LOGO>";
        final String textKeyCode = "<CODE>";
        final String textKeyURLBase = "<URLBASE>";
        final String textPerfil = "Recuerda llenar tu perfil";

        String urlBaseFront = serviceProperties.getURLServerFront(); //"http://localhost:8000/";
        String urlLogo = serviceProperties.getURLLogo(); //"http://www.posmagroup.com/assets/images/logoPosma.png";

/*        String validationText = "<h4>Validaci&oacute;n de cuenta skola</h4><br/>" +
                "Su cuenta fue creada exitosamente <br> " +
                "Para completar el registro, nacesitamos validar su cuenta <br/> Por favor has click en el siguiente enlace:<br> " +
                "<a href=\"http://<IPHOST>:8080/skola/user/validate?code=<code>\"> http://<IPHOST>:8080/skola/user/validate?code=<code></a> <br> " +
                "Si en link no funciona, copia el enlace y pegalo en una nueva ventada del browser";

                String validationText2 = "<h4>Validaci&oacute;n de cuenta skola</h4><br/>" +
                "Su cuenta fue creada exitosamente <br> " +
                "Para completar el registro, nacesitamos validar su cuenta <br/> Por favor has click en el siguiente enlace:<br> " +
                "<a href=\""+urlLogin+"/#/user/validate?code="+textKeyCode+"\"> "+urlLogin+"/#/user/validate?code="+textKeyCode+"></a> <br> " +
                "Si en link no funciona, copia el enlace y pegalo en una nueva ventada del browser";
        */

        String validationText = "<table border=0 cellspacing=0 cellpadding=0 width=512 bgcolor=#000000\n" +
                "       style=\"background-color:#f0f0f0;margin:0 auto;max-width:512px;width:inherit\">\n" +
                "    <tbody>\n" +
                "    <tr>\n" +
                "        <td bgcolor=\"#F6F8FA\" style=\"background-color:#f6f8fa;padding:28px 0 20px 0\">\n" +
                "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"\n" +
                "                   style=\"width:100%!important;min-width:100%!important\">\n" +
                "                <tbody>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" width=\"100%\">\n" +
                "                        <a href=\""+textKeyURLBase+"\"\n" +
                "                           style=\"color:#008cc9;display:inline-block;text-decoration:none\" target=\"_blank\" data-saferedirecturl=\""+urlBaseFront+"\">\n" +
                "                            <img alt=\"Skola\" border=\"0\" src=\""+textKeyLogo+"\" height=\"60\" width=\"68\"\n" +
                "                                 style=\"outline:none;color:#ffffff;text-decoration:none\">\n" +
                "                        </a>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" width=\"100%\" style=\"padding:16px 24px 0 24px\">\n" +
                "                        <h2 style=\"margin:0;color:#262626;font-weight:200;font-size:20px;padding-bottom:5px;line-height:1.2\">Validaci&oacute;n de cuenta Skola</h2>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                </tbody>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td>\n" +
                "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"margin:0 10px;max-width:492px\">\n" +
                "                <tbody>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" style=\"padding:25px 0;text-align:center\">\n" +
                "                        <p style=\"margin:0;color:#262626;font-weight:100;font-size:16px;padding-bottom:15px;line-height:1.167\">Su cuenta Skola fue creada<br/>\n" +
                "                            Para completar el registro, tienes 24 horas para validar su cuenta<br/>\n" +
                "                            Por favor has click en el siguiente enlace:<br/></p>\n" +
                "                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"display:inline-block\">\n" +
                "                            <tbody>\n" +
                "                            <tr>\n" +
                "                                <td align=\"center\" valign=\"middle\">\n" +
                "                                    <a href=\""+textKeyURLBase+"\"\n" +
                "                                       style=\"word-wrap:normal;color:#008cc9;word-break:normal;white-space:nowrap;display:block;text-decoration:none\" target=\"_blank\">\n" +
                "                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"auto\">\n" +
                "                                            <tbody>\n" +
                "                                            <tr>\n" +
                "                                                <td bgcolor=\"#008CC9\" style=\"padding:6px 16px;color:#ffffff;;font-weight:bold;font-size:16px;border-color:#008cc9;background-color:#008cc9;border-radius:2px;border-width:1px;border-style:solid\">\n" +
                "                                                    <a href=\""+textKeyURLBase+"#/user/validate?code="+textKeyCode+"\" style=\"color:#dff0d8;text-decoration:none\"> "+textKeyURLBase+"#/user/validate?code="+textKeyCode+"</a> <br>\n" +
                "                                                </td>\n" +
                "                                            </tr>\n" +
                "                                            </tbody>\n" +
                "                                        </table>\n" +
                "                                    </a>\n" +
                "                                </td>\n" +
                "                            </tr>\n" +
                "                            </tbody>\n" +
                "                        </table>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                </tbody>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    </tbody>\n" +
                "</table>;\n";


        try{
            if(userDao.findByMail(userDto.getMail().toUpperCase()) != null){
                messageDto.setSuccess(false);
                messageDto.setMessage("Already exists user with the mail " + userDto.getMail());
                return messageDto;
            }
            if (userDto != null && userDto.getMail() != null && userDto.getMail().trim() != "") {
                UserEntity userEntity = entityMapper.boToEntity(userDto, new UserEntity());

                userEntity.setMail(userDto.getMail().toUpperCase());
                userEntity.setPassword(DigestUtils.shaHex(userDto.getPassword()));
                userEntity.setStatus(StatusUser.TO_VALIDATE);

                // Set role default TEACHER id (1)
                RoleEntity roleEntity = roleDao.findOne(1);
                userEntity.addRoleList(roleEntity);

                // crea el usuario
                userEntity = userDao.create(userEntity);

                Date date = Calendar.getInstance().getTime();
                String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(date);
                String validationCode = encrypt.getHash(encrypt.MD5,timeStamp + userDto.getMail());
                ValidationAccountEntity validationAccountEntity = new ValidationAccountEntity();
                validationAccountEntity.setUserId(userEntity);
                validationAccountEntity.setDate(date);
                validationAccountEntity.setValidationCode(validationCode);
                // crea la entrada para la posterior validacion del correo Electronico
                validationAccountDao.create(validationAccountEntity);

                //Preparar contenido del correo
                validationText = validationText.replaceAll(textKeyCode,validationCode);
                validationText = validationText.replaceAll(textKeyURLBase, urlBaseFront);
                validationText = validationText.replaceAll(textKeyLogo, urlLogo);

                NotificationEntity notificationEntity = new NotificationEntity();
                notificationEntity.setText(textPerfil);
                notificationEntity.setStatus(NotificationStatus.SENDED);
                notificationEntity.setActive(true);
                notificationEntity.setTypeName("Perfil");
                notificationEntity.setDate(date);
                notificationEntity.setType(NotificationType.INFO);
                notificationEntity.setUser(userEntity);

                notificationDao.create(notificationEntity);

                NotificationDto notificationDto = new NotificationDto();
                notificationDto.setText(validationText);
                notificationDto.setTitle("Correo de validacion de mail");
                notificationDto.setTo(userDto.getMail());
                // Envia correo para la validacion de cuenta

                messageDto.setResponse(entityMapper.entityToBO(userEntity));
                notificationBF.sendNotification(notificationDto);

            }else{
                messageDto.setSuccess(false);
                messageDto.setMessage("Bad input to send mail, It was not possible to send the mail");
            }

        }catch (DataIntegrityViolationException dve){
            dve.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage("Already exists user with the mail " + userDto.getMail());
            new Exception(messageDto.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage("Internal Exception, please contact yuor administrator");
            new Exception(messageDto.getMessage());
        }

        return messageDto;
    }


    /**
     * Service to validate mail account from user
     *
     * @author FrancisRies
     * @version 1.0
     * @since 27/03/2017
     */
    @Override
    @Transactional
    public MessageDto userValidate(String code) {

        MessageDto messageDto = new MessageDto();
        messageDto.setMessage("User Validate");

        ValidationAccountEntity validationAccountEntity;

        try {
            //falta validar codigo de entidad regitry
            validationAccountEntity = validationAccountDao.findByValidationCode(code);

            if(validationAccountEntity == null) {
                messageDto.setSuccess(false);
                messageDto.setMessage("The code is not valid, this may be that the code has already been used or does not exist");
            }else{
                // Valida si el codigo no esta caducado
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(validationAccountEntity.getDate()); // Configuramos la fecha que se recibe
                calendar.add(Calendar.DAY_OF_YEAR, day);

                Date date = Calendar.getInstance().getTime();
                if(date.after(calendar.getTime())){
                    messageDto.setMessage("The code is no longer valid");
                    messageDto.setSuccess(false);
                }else{
                    UserEntity userEntity = validationAccountEntity.getUserId();
                    userEntity.setStatus(StatusUser.ACTIVE);

                    userDao.update(userEntity);

                    MessageDto messageDto1 = wizardBf.save(userEntity);
                    if(messageDto1.getSuccess())
                        messageDto.setResponse(entityMapper.entityToBO(userEntity));
                    else
                        new Exception("Error al crear la entrada en tabla de wizard");

                }
                // Borra entrada para limpieza de tabla
                validationAccountDao.delete(validationAccountEntity);

            }
        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage("The code is not valid, this may be that the code has already been used or does not exist");
        }

        return messageDto;
    }


    /**
     * Service to validate mail account from user
     * @since 10/04/2017
     * @param mail mail that requires recovering password
     * @author FrancisRies
     * @version 1.0
     */

    @Override
    @Transactional
    public MessageDto restorePassword(String mail){
        MessageDto messageDto = new MessageDto();
        messageDto.setMessage("User Validate");

        // variable para plantilla de mail:
        final String TEXT_KEY_LOGO = "<LOGO>";
        final String TEXT_KEY_URL_LOGIN = "<LOGIN>";
        final String TEXT_KEY_PASSWORD = "<PASSWORD>";

        // valores para plantilla
        String urlBaseFront = serviceProperties.getURLServerFront(); //"http://localhost:8000/";
        String urlLogo = serviceProperties.getURLLogo(); //"http://www.posmagroup.com/assets/images/logoPosma.png";

        //String recoverText = "<h4>Recuperaci&oacute;n de clave skola</h4><br/>" +
        //        "<br> " +
        //        "Su clave es: <br>" +
        //        "<div style='background-color=#d4e0fa'>"+TEXT_KEY_PASSWORD+"</div> ";

        String recoverText = "<table border=0 cellspacing=0 cellpadding=0 width=512 bgcolor=#000000\n" +
                "       style=\"background-color:#f0f0f0;margin:0 auto;max-width:512px;width:inherit\">\n" +
                "    <tbody>\n" +
                "    <tr>\n" +
                "        <td bgcolor=\"#F6F8FA\" style=\"background-color:#f6f8fa;padding:28px 0 20px 0\">\n" +
                "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"\n" +
                "                   style=\"width:100%!important;min-width:100%!important\">\n" +
                "                <tbody>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" width=\"100%\">\n" +
                "                        <a href=\""+TEXT_KEY_URL_LOGIN+"\"\n" +
                "                           style=\"color:#008cc9;display:inline-block;text-decoration:none\" target=\"_blank\" data-saferedirecturl=\""+TEXT_KEY_URL_LOGIN+"\">\n" +
                "                            <img alt=\"Skola\" border=\"0\" src=\""+TEXT_KEY_LOGO+"\" height=60\" width=\"68\"\n" +
                "                                 style=\"outline:none;color:#ffffff;text-decoration:none\">\n" +
                "                        </a>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" width=\"100%\" style=\"padding:16px 24px 0 24px\">\n" +
                "                        <h2 style=\"margin:0;color:#262626;font-weight:200;font-size:20px;padding-bottom:5px;line-height:1.2\">Recuperaci&oacute;n de Clave</h2>\n" +
                "                        <p style=\"margin:0;color:#4c4c4c;font-weight:400;font-size:16px;line-height:1.5\">\n" +
                "                            Su nueva clave es:\n" +
                "                        </p>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                </tbody>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td>\n" +
                "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"margin:0 10px;max-width:492px; min-width: 300px\">\n" +
                "                <tbody>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" style=\"padding:25px 0;text-align:center\">\n" +
                "                        <p style=\"margin:0;color:#262626;font-weight:100;font-size:18px;padding-bottom:15px;line-height:1.167\">"+TEXT_KEY_PASSWORD+"</p>\n" +
                "                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"display:inline-block\">\n" +
                "                            <tbody>\n" +
                "                            <tr>\n" +
                "                                <td align=\"center\" valign=\"middle\">\n" +
                "                                    <a href=\"\"+TEXT_KEY_URL_LOGIN+\"\"\n" +
                "                                       style=\"word-wrap:normal;color:#008cc9;word-break:normal;white-space:nowrap;display:block;text-decoration:none\" target=\"_blank\">\n" +
                "                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"auto\">\n" +
                "                                            <tbody>\n" +
                "                                            <tr>\n" +
                "                                                <td bgcolor=\"#008CC9\" style=\"padding:6px 16px;color:#ffffff;;font-weight:bold;font-size:16px;border-color:#008cc9;background-color:#008cc9;border-radius:2px;border-width:1px;border-style:solid\">\n" +
                "                                                    <a href=\""+TEXT_KEY_URL_LOGIN+"\" style=\"color:#dff0d8;text-decoration:none\"> SKOLA </a>\n" +
                "                                                </td>\n" +
                "                                            </tr>\n" +
                "                                            </tbody>\n" +
                "                                        </table>\n" +
                "                                    </a>\n" +
                "                                </td>\n" +
                "                            </tr>\n" +
                "                            </tbody>\n" +
                "                        </table>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                </tbody>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    </tbody>\n" +
                "</table>;\n";


        String newPassword = getCadenaAlfanumAleatoria(8);

        try{
            if (mail != null) {
                UserEntity userEntity = userDao.findByMail(mail.toUpperCase());
                userEntity.setPassword(DigestUtils.shaHex(newPassword));

                userEntity = userDao.update(userEntity);

                if(userEntity == null){
                    messageDto.setSuccess(false);
                    messageDto.setMessage("Mail " + mail + " don't exit");
                    return messageDto;
                }
                recoverText = recoverText.replaceAll(TEXT_KEY_PASSWORD, newPassword);
                recoverText = recoverText.replaceAll(TEXT_KEY_URL_LOGIN, urlBaseFront);
                recoverText = recoverText.replaceAll(TEXT_KEY_LOGO, urlLogo);

                NotificationDto notificationDto = new NotificationDto();
                notificationDto.setText(recoverText);
                notificationDto.setTitle("Recuperacion de clave skola");
                notificationDto.setTo(mail);
                // Envia correo para la validacion de cuenta

                UserDto userDto = new UserDto();

                userDto = entityMapper.entityToBO(userEntity);

                messageDto.setResponse(userDto);
                notificationBF.sendNotification(notificationDto);

            }else{
                messageDto.setSuccess(false);
                messageDto.setMessage("Bad input to send mail, It was not possible to send the mail");
            }

        }catch (DataIntegrityViolationException dve){
            dve.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage("Mail " + mail + " don't exit");
            new Exception(messageDto.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage("Internal Exception, please contact yuor administrator");
            new Exception(messageDto.getMessage());
        }

        return messageDto;
    }


    private String getCadenaAlfanumAleatoria (int longitud){
        String cadenaAleatoria = "";
        long milis = new GregorianCalendar().getTimeInMillis();
        Random r = new Random(milis);
        int i = 0;
        while ( i < longitud){
            char c = (char)r.nextInt(255);
            if ( (c >= '0' && c <='9') || (c >='A' && c <='Z') || (c >='a' && c <='z') ){
                cadenaAleatoria += c;
                i ++;
            }
        }
        return cadenaAleatoria;
    }








    /**
     * Service to update data from user
     * @since 19/04/2017
     * @param userId user identifier
     * @param userDto user dto with all params to update
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto userUpdate(Long userId, UserDto userDto){
        MessageDto messageDto = new MessageDto();

        UserEntity userEntity;

        try {
            // busca los datos del usuario en la BD
            userEntity = userDao.findOne(userId);

            if(userEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            userEntity = entityMapper.boToEntity(userDto, userEntity);

            userEntity = userDao.update(userEntity);

            MessageDto notification = notificationBF.getHistoryNotificationByInfo(userDto.getMail());
            NotificationHistoryDto historyDto = (NotificationHistoryDto) notification.getResponse();
            if(historyDto.getNotifications().size() > 0)
                notificationBF.removeActiveNotification(historyDto.getNotifications().get(0).getId());

            messageDto.setResponse(entityMapper.entityToBO(userEntity));
            messageDto.setMessage(systemMessage.getMessage("SK-000"));

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
        }catch (PersistenceException pe){
            pe.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-003");
            messageDto.setMessage(systemMessage.getMessage("SK-003"));
        }

        return messageDto;
    }


    /**
     * Service query all users with role (STUDENT/TEACHER/...) from a community
     * @since 24/04/2017
     * @param roleId role identifier
     * @param communityId community identifier
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto getAllUsersByRoleCommunity(Long roleId, Long communityId){

        MessageDto messageDto = new MessageDto();
        List<UserDto> userDtoList = new ArrayList<UserDto>();

        try {
            List<UserRoleCommunityEntity> userRoleCommunityList = userRoleCommunityDao.findAllByTwoFieldsId("COMMUNITY_ID", communityId, "ROLE_ID", roleId);

            for (UserRoleCommunityEntity userRoleCommunity : userRoleCommunityList) {
                userDtoList.add(entityMapper.entityToBO(userRoleCommunity.getUser()));
            }
            messageDto.setResponse(userDtoList);

        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("GET-COM-USER-001");
            messageDto.setMessage(systemMessage.getErrorCode().get("GET-COM-USER-001"));
        }

        return messageDto;
    }



    /**
     * Service query all users with role (STUDENT/TEACHER/...) from a community
     * @since 24/04/2017
     * @param userDto detail user to create
     * @param roleId role identifier
     * @param communityId community identifier
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto userCreateInCommunity(Long communityId, Long roleId, UserDto userDto){
        MessageDto messageDto = new MessageDto();
        messageDto.setMessage("User created");

        try {

            if(userDto.getMail() != null) {
                if (userDao.findByMail(userDto.getMail()) != null) {
                    messageDto.setSuccess(false);
                    messageDto.setMessage("Already exists user with the mail " + userDto.getMail());
                    return messageDto;
                }
            }

            UserEntity userEntity = entityMapper.boToEntity(userDto, new UserEntity());
            userEntity.setStatus(StatusUser.UNASSIGNED);

            if (userDto != null && userDto.getMail() != null && userDto.getMail().trim() != "") {
                userEntity.setMail(userDto.getMail().trim().toUpperCase());
            }

            if (userDto != null && userDto.getMailRepresentative() != null && userDto.getMailRepresentative().trim() != ""){
                userEntity.setMailRepresentative(userDto.getMailRepresentative().trim().toUpperCase());
            }

            // Set role TEACHER (1) - STUDENT (2) - ...
            RoleEntity roleEntity = roleDao.findOne(roleId);
            userEntity.addRoleList(roleEntity);

            // Crea el usuario
            userEntity = userDao.create(userEntity);

            if(userEntity != null && userEntity.getId() != null){
                RequestAssociateDto requestAssociateDto = new RequestAssociateDto(true);
                messageDto = communityUserBF.setCommunityIdUserIdRoleId(communityId, userEntity.getId(), roleId, requestAssociateDto);

                if(messageDto.getSuccess()){
                    messageDto.setResponse(true);

                }


            }else{
                new Exception(systemMessage.getMessage("SK-001"));
            }
        }catch (DataIntegrityViolationException dve){
            dve.printStackTrace();
            new Exception(systemMessage.getMessage("SK-005"));
        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage("Internal Exception, please contact yuor administrator");
            new Exception(messageDto.getMessage() + "\n " + e.getMessage());
        }

        return messageDto;

    }


    /**
     * Service query all users with role (STUDENT) from a community and not in Section (sectionId)
     * @since 25/04/2017
     * @param sectionId role identifier
     * @param communityId community identifier
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    public MessageDto getAllUsersByRoleCommunityMinusSection(Long sectionId, Long communityId){

        MessageDto messageDto = new MessageDto();
        List<UserDto> userDtoList = new ArrayList<UserDto>();

        try {
            List<UserEntity> userEntityList = userRoleCommunityDao.findAllMinusSection(communityId, sectionId);

            for (UserEntity userEntity : userEntityList) {
                userDtoList.add(entityMapper.entityToBO(userEntity));
            }
            messageDto.setResponse(userDtoList);

        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("GET-COM-USER-001");
            messageDto.setMessage(systemMessage.getErrorCode().get("GET-COM-USER-001"));
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto getUser(Long userId){
        MessageDto messageDto = new MessageDto();

        UserEntity userEntity;

        try {
            // busca los datos del usuario en la BD
            userEntity = userDao.findOne(userId);

            if(userEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            messageDto.setResponse(entityMapper.entityToBOFull(userEntity));
            messageDto.setMessage(systemMessage.getMessage("SK-000"));

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
        }catch (PersistenceException pe){
            pe.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-003");
            messageDto.setMessage(systemMessage.getMessage("SK-003"));
        }

        return messageDto;
    }


    @Override
    public MessageDto getUser(String userName){
        MessageDto messageDto = new MessageDto();

        UserEntity userEntity;

        try {
            // busca los datos del usuario en la BD
            userEntity = userDao.findByMail(userName.toUpperCase());

            if(userEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            messageDto.setResponse(entityMapper.entityToBO(userEntity));
            messageDto.setMessage(systemMessage.getMessage("SK-000"));

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
        }catch (PersistenceException pe){
            pe.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-003");
            messageDto.setMessage(systemMessage.getMessage("SK-003"));
        }

        return messageDto;
    }

    @Override
    public MessageDto savePhotoUser(Long userId, MultipartFile photo){
        MessageDto messageDto = new MessageDto();

        OutputStream out = null;
        InputStream filecontent = null;

        try {

            UserEntity userEntity = userDao.findOne(userId);
            PhotoDirectionEntity photoDirectionEntity = photoDirectionDao.findOne(1);
            //Prueba
            //out = new FileOutputStream(new File("W:\\Documents\\Repositorio de Ricardo\\Skola\\skola-frontend\\app\\static\\images\\"+ File.separator + photo.getOriginalFilename()));
            File fileImage = new File(photoDirectionEntity.getDireccion() +userEntity.getId() +photo.getOriginalFilename());
            out = new FileOutputStream(fileImage);
            filecontent = photo.getInputStream();

            int read = 0;
            final byte[] bytes = new byte[1024];

            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            userEntity.setFoto(userEntity.getId() +photo.getOriginalFilename());
            messageDto.setResponse(entityMapper.entityToBO(userEntity));

            if(out != null)
                out.close();

            if(filecontent != null)
                filecontent.close();

            Runtime.getRuntime().exec("chmod 777 " + fileImage.getAbsolutePath());
            System.out.print("------------- \n "+fileImage.getAbsolutePath());
        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-003");
            messageDto.setMessage(systemMessage.getErrorCode().get("SK-003"));
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto userCreateFacebook(UserDto userDto){
        MessageDto messageDto = new MessageDto();
        final String textPerfil = "Recuerda llenar tu perfil";

        try{
            UserEntity userFind = userDao.findByMail(userDto.getMail().toUpperCase());
            if( userFind != null){
                NetworksEntity networksEntity = new NetworksEntity();
                networksEntity.setNetworks("FACEBOOK");
                networksEntity.setPassword(DigestUtils.shaHex(userDto.getPassword()));
                networksEntity.setUser(userFind);
                networkDao.create(networksEntity);
                messageDto.setResponse(userFind);
                return messageDto;
            }
            UserEntity userEntity = entityMapper.boToEntity(userDto, new UserEntity());

            userEntity.setMail(userDto.getMail().toUpperCase());
            //userEntity.setPassword(DigestUtils.shaHex(userDto.getPassword()));
            userEntity.setStatus(StatusUser.ACTIVE);

            // Set role default TEACHER id (1)
            RoleEntity roleEntity = roleDao.findOne(1);
            userEntity.addRoleList(roleEntity);

            userEntity = userDao.create(userEntity);

            NetworksEntity networksEntity = new NetworksEntity();
            networksEntity.setNetworks("FACEBOOK");
            networksEntity.setPassword(DigestUtils.shaHex(userDto.getPassword()));
            networksEntity.setUser(userEntity);

            networkDao.create(networksEntity);

            Date date = Calendar.getInstance().getTime();

            NotificationEntity notificationEntity = new NotificationEntity();
            notificationEntity.setText(textPerfil);
            notificationEntity.setStatus(NotificationStatus.SENDED);
            notificationEntity.setActive(true);
            notificationEntity.setTypeName("Perfil");
            notificationEntity.setDate(date);
            notificationEntity.setType(NotificationType.INFO);
            notificationEntity.setUser(userEntity);

            notificationDao.create(notificationEntity);

            messageDto.setResponse(userEntity);


        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-003");
            messageDto.setMessage(systemMessage.getErrorCode().get("SK-003"));
        }

        return messageDto;
    }

}

