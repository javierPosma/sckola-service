package com.posma.sckola.app.util;

/**
 * Created by Francis Ries on 27/03/2017.
 */
public class ServiceProperties  {

    private final String URLServer;
    private final String URLServerFront;
    private final String URLLogo;

    public ServiceProperties(String urlBusiness, String urlFront, String urlLogo){
        URLServer = urlBusiness;
        URLServerFront = urlFront;
        URLLogo = urlLogo;
    }

    /**
     * Service to get URL where server is running
     * @author FrancisRies
     * @version 1.0
     * @since 23/05/2017
     */
    public String getURLServer () {
        return URLServer;
    }

    /**
     * Service to get URL where server Front is running
     * @author FrancisRies
     * @version 1.0
     * @since 23/05/2017
     */
    public String getURLServerFront () {
        return URLServerFront;
    }

    /**
     * Service to get URL where server Front is running
     * @author FrancisRies
     * @version 1.0
     * @since 23/05/2017
     */
    public String getURLLogo () {
        return URLLogo;
    }

}

