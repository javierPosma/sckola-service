package com.posma.sckola.app.util;

/**
 * Created by Posma Group on 05/09/2018.
 */
public class NotificationMessage {

    private final String profile_request;
    private final String profile_success;
    private final String community_request;
    private final String community_success;
    private final String evaluationPlan_request;
    private final String evaluationPlan_success;
    private final String section_request;
    private final String section_success;
    private final String subject_request;
    private final String subject_success;
    private final String students_request;
    private final String students_success;
    private final String calification_request;
    private final String calification_success;
    private final String attendance_request;
    private final String attendance_success;


    private final String profile;
    private final String community;
    private final String EvaluationPlan;
    private final String section;
    private final String subject;
    private final String students;

    private final String califications;



    private final String attendance;

    public String getCommunity_request() {
        return community_request;
    }

    public String getCommunity_success() {
        return community_success;
    }

    public String getEvaluationPlan_request() {
        return evaluationPlan_request;
    }

    public String getEvaluationPlan_success() {
        return evaluationPlan_success;
    }

    public String getCalification_request() {
        return calification_request;
    }

    public String getCalification_success() {
        return calification_success;
    }

    public String getAttendance_request() {
        return attendance_request;
    }

    public String getAttendance_success() {
        return attendance_success;
    }

    public String getProfile_request() {
        return profile_request;
    }

    public String getProfile_success() {
        return profile_success;
    }

    public String getStudents_request() {
        return students_request;
    }

    public String getSection_request() {
        return section_request;
    }

    public String getSection_success() {
        return section_success;
    }

    public String getSubject_request() {
        return subject_request;
    }

    public String getSubject_success() {
        return subject_success;
    }

    public String getStudents_success() {
        return students_success;
    }

    public String getProfile() {
        return profile;
    }

    public String getCommunity() {
        return community;
    }

    public String getEvaluationPlan() {
        return EvaluationPlan;
    }

    public String getSection() {
        return section;
    }

    public String getSubject() {
        return subject;
    }

    public String getStudents() {
        return students;
    }

    public String getAttendance() {
        return attendance;
    }
    public String getCalifications() {
        return califications;
    }



    public NotificationMessage(
            String profile_request,
            String profile_success,
            String community_request,
            String community_success,
            String evaluationPlan_request,
            String evaluationPlan_success,
            String section_request,
            String section_success,
            String subject_request,
            String subject_success,
            String students_request,
            String students_success,
            String score_request,
            String score_success,
            String attendance_request,
            String attendance_success,
            String profile, String community, String evaluationPlan, String section, String subject, String students, String attendance,String califications){

        this.profile_request = profile_request;
        this.profile_success = profile_success;
        this.community_request = community_request;
        this.community_success = community_success;
        this.evaluationPlan_request = evaluationPlan_request;
        this.evaluationPlan_success = evaluationPlan_success;
        this.section_request = section_request;
        this.section_success = section_success;
        this.subject_request = subject_request;
        this.subject_success = subject_success;
        this.students_request = students_request;
        this.students_success = students_success;
        this.calification_request = score_request;
        this.calification_success = score_success;
        this.attendance_request = attendance_request;
        this.attendance_success = attendance_success;
        this.profile = profile;
        this.community = community;
        this.EvaluationPlan = evaluationPlan;
        this.section = section;
        this.subject = subject;
        this.students = students;
        this.attendance = attendance;
        this.califications = califications;
    }


}

