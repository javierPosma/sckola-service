package com.posma.sckola.app.util;

import com.posma.sckola.app.dto.*;
import com.posma.sckola.app.persistence.dao.OrganizationDao;
import com.posma.sckola.app.persistence.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis on 27/03/2017.
 */
@Service
public class EntityMapper {

    @Autowired
    OrganizationDao organizationDao;

    @Autowired
    Validation validation;

    public UserDto entityToBO(UserEntity userEntity) {
        UserDto userDto = new UserDto();
        userDto.setId(userEntity.getId());
        userDto.setIdentification(userEntity.getIdentification());
        userDto.setFirstName(userEntity.getFirstName());
        userDto.setSecondName(userEntity.getSecondName());
        userDto.setLastName(userEntity.getLastName());
        userDto.setSecondLastName(userEntity.getSecondLastName());
        userDto.setMail(userEntity.getMail());
        userDto.setStatus(userEntity.getStatus().name());
        userDto.setCurriculum(entitysToBO(userEntity.getCurriculumList()));
        //if(userEntity.getNetwork() != null) {
        //    for (NetworksEntity networksEntity : userEntity.getNetwork()) {
        //        userDto.addNetworksList(entityToBO(networksEntity));
        //    }
        //}
        if (userEntity.getBirthdate() != null)
            userDto.setBirthdate(validation.dateToString(userEntity.getBirthdate()));
        if (userEntity.getGender() != null)
            userDto.setGender(userEntity.getGender().name());
        /*
        if (userEntity.getRoleList().size() > 0){
            for (RoleEntity roleEntity : userEntity.getRoleList()) {
                userDto.addRoleList(entityToBO(roleEntity));
            }
        }
        */
        if(userEntity.getFoto() != null){
            userDto.setFoto(userEntity.getFoto());
        }
        return userDto;
    }

    /**
     * Convierte una userDTO a UserEntity, como todos los campos no son actualizables, se debe enviar el UserEntity existente en la BD
     * @param userDto
     * @param userEntity
     * @return UserEntity con el merge del DTO y lo guardado en la BD
     */
    public UserEntity boToEntity(UserDto userDto, UserEntity userEntity) {
        userEntity.setFirstName(userDto.getFirstName() != null ? userDto.getFirstName().toUpperCase() : null);
        userEntity.setSecondName(userDto.getSecondName() != null ? userDto.getSecondName().toUpperCase() : null);
        userEntity.setLastName(userDto.getLastName() != null ? userDto.getLastName().toUpperCase() : null);
        userEntity.setSecondLastName(userDto.getSecondLastName() != null ? userDto.getSecondLastName().toUpperCase() : null);
        userEntity.setIdentification(userDto.getIdentification());
        //if(userDto.getNetworks() != null) {
        //    for (NetworksDto networksDto : userDto.getNetworks()) {
        //        userEntity.addNetworksList(boToEntity(networksDto));
        //    }
        //}
        if(userDto.getFoto() != null)
            userEntity.setFoto(userDto.getFoto());
        if(userDto.getBirthdate() != null)
            userEntity.setBirthdate(validation.stringToDate(userDto.getBirthdate()));
        if(Gender.MALE.name().equalsIgnoreCase(userDto.getGender()))
            userEntity.setGender(Gender.MALE);
        if(Gender.FEMALE.name().equalsIgnoreCase(userDto.getGender()))
            userEntity.setGender(Gender.FEMALE);
        return userEntity;
    }


    /**
     * Convierte una evaluationDto a evaluationEntity, como todos los campos no son actualizables, se debe enviar el evaluationEntity existente en la BD
     * @param evaluationDto
     * @param evaluationEntity
     * @return EvaluationEntity con el merge del DTO y lo guardado en la BD
     */
    public EvaluationEntity boToEntity(EvaluationDto evaluationDto, EvaluationEntity evaluationEntity) {
        evaluationEntity.setWeight(evaluationDto.getWeight());
        evaluationEntity.setObjective(evaluationDto.getObjective() != null ? evaluationDto.getObjective().toUpperCase() : null);
        if(evaluationDto.getDate() != null)
            evaluationEntity.setDate(validation.stringToDate(evaluationDto.getDate()));
        return evaluationEntity;
    }


    public OrganizationDto entityToBO(OrganizationEntity organizationEntity) {
        OrganizationDto organizationDto = new OrganizationDto();
        organizationDto.setId(organizationEntity.getId());
        organizationDto.setTaxIdentity(organizationEntity.getTaxIdentity());
        organizationDto.setBusinessName(organizationEntity.getBusinessName());
        organizationDto.setAddress(organizationEntity.getAddress());
        organizationDto.setEmail(organizationEntity.getEmail());

        return organizationDto;
    }


    public CommunityDto entityToBO(CommunityUserEntity communityUserEntity){
        if(communityUserEntity == null)
            return null;
        CommunityDto communityDto = new CommunityDto();
        communityDto.setId(communityUserEntity.getId());
        communityDto.setCommunityOrigin(entityToBO(communityUserEntity.getCommunity()));
        communityDto.setName(communityUserEntity.getName());
        communityDto.setDescription(communityUserEntity.getDescription());

        return communityDto;
    }

    public CommunityDto entityToBO(CommunityEntity communityEntity){
        if(communityEntity == null)
            return null;
        CommunityDto communityDto = new CommunityDto();
        communityDto.setId(communityEntity.getId());
        communityDto.setName(communityEntity.getName());
        communityDto.setDescription(communityEntity.getDescription());

        return communityDto;
    }

    public List<CurriculumDto> entitysToBO(List<CurriculumEntity> curriculumEntities){
        List<CurriculumDto> curriculumDtos = new ArrayList<CurriculumDto>();
        for (CurriculumEntity curriculumEntity: curriculumEntities){
            curriculumDtos.add(entityToBO(curriculumEntity));
        }
        return curriculumDtos;
    }

    public CurriculumDto entityToBO(CurriculumEntity curriculumEntity){
        if(curriculumEntity == null)
            return null;
        CurriculumDto curriculumDto = new CurriculumDto();
        curriculumDto.setId(curriculumEntity.getId());
        curriculumDto.setTitle(curriculumEntity.getTitle());
        curriculumDto.setInstitute(curriculumEntity.getInstitute());
        curriculumDto.setCommunity(entityToBO(curriculumEntity.getCommunity()));
        curriculumDto.setDate(validation.dateToString(curriculumEntity.getDate()));
        if(curriculumEntity.getUser() != null)
            curriculumDto.setUserId(curriculumEntity.getUser().getId());
        return curriculumDto;
    }

    public MatterDto entityToBO(MatterEntity matterEntity){
        MatterDto matterDto = new MatterDto();
        matterDto.setId(matterEntity.getId());
        matterDto.setName(matterEntity.getName());
        matterDto.setDescription(matterEntity.getDescription());
        matterDto.setObjective(matterEntity.getObjective());
        matterDto.setEspecificObjective(matterEntity.getEspecificObjective());
        return matterDto;
    }

    public MatterCommunityDto entityToBO(MatterCommunityEntity matterCommunityEntity){
        MatterCommunityDto matterCommunityDto = new MatterCommunityDto();
        matterCommunityDto.setId(matterCommunityEntity.getId());
        matterCommunityDto.setName(matterCommunityEntity.getName());
        matterCommunityDto.setCode(matterCommunityEntity.getCode());
        if(matterCommunityEntity.getMatter() != null)
            matterCommunityDto.setMatterId(matterCommunityEntity.getMatter().getId());
        if(matterCommunityEntity.getCommunity() != null) {
            matterCommunityDto.setCommunityId(matterCommunityEntity.getCommunity().getId());
            matterCommunityDto.setCommunityName(matterCommunityEntity.getCommunity().getName());
        }
        matterCommunityDto.setDescription(matterCommunityEntity.getDescription());
        matterCommunityDto.setObjective(matterCommunityEntity.getObjective());
        matterCommunityDto.setEspecificObjective(matterCommunityEntity.getEspecificObjective());
        return matterCommunityDto;
    }


    public SectionDto entityToBOFull(SectionEntity sectionEntity){
        SectionDto sectionDto = new SectionDto();
        sectionDto.setId(sectionEntity.getId());
        sectionDto.setName(sectionEntity.getName());
        sectionDto.setComunityId(sectionEntity.getCommunity().getId());

       // Listado de estudiantes por seccion
        if(sectionEntity.getStudentList() != null){
            List<UserDto> userDtoList = new ArrayList<UserDto>(sectionEntity.getStudentList().size());
            for(UserEntity userEntity : sectionEntity.getStudentList()){
                userDtoList.add(entityToBO(userEntity));
            }
            sectionDto.setStudentList(userDtoList);
        }
        return sectionDto;
    }

    public SectionDto entityToBO(SectionEntity sectionEntity){
        SectionDto sectionDto = new SectionDto();
        sectionDto.setId(sectionEntity.getId());
        sectionDto.setName(sectionEntity.getName());

        if(sectionEntity.getStudentList() != null){
            sectionDto.setCountStudent(Long.valueOf(sectionEntity.getStudentList().size()));
        }else{
            sectionDto.setCountStudent(Long.valueOf(0));
        }
        sectionDto.setComunityId(sectionEntity.getCommunity().getId());

        return sectionDto;
    }

    public MatterCommunitySectionDto entityToBO(MatterCommunitySectionEntity matterCommunitySectionEntity){
        if(matterCommunitySectionEntity == null)
            return null;
        MatterCommunitySectionDto matterCommunitySectionDto = new MatterCommunitySectionDto();
        matterCommunitySectionDto.setId(matterCommunitySectionEntity.getId());
        matterCommunitySectionDto.setTeacherId(matterCommunitySectionEntity.getUser().getId());
        matterCommunitySectionDto.setType(matterCommunitySectionEntity.getType().name());
        if(matterCommunitySectionEntity.getMatterCommunity() != null)
            matterCommunitySectionDto.setMatterCommunity(entityToBO(matterCommunitySectionEntity.getMatterCommunity()));
        if(matterCommunitySectionEntity.getSection() != null)
            matterCommunitySectionDto.setSection(entityToBO(matterCommunitySectionEntity.getSection()));
        if(matterCommunitySectionEntity.getEvaluationPlan() != null)
            matterCommunitySectionDto.setEvaluationPlan(entityToBO(matterCommunitySectionEntity.getEvaluationPlan()));
        return matterCommunitySectionDto;
    }

    public RoleDto entityToBO(RoleEntity roleEntity){
        RoleDto roleDto = new RoleDto();
        roleDto.setId(roleEntity.getId());
        roleDto.setName(roleEntity.getName());
        roleDto.setDescription(roleEntity.getDescription());
        return roleDto;
    }

    public EvaluationToolDto entityToBO(EvaluationToolEntity evaluationToolEntity){
        EvaluationToolDto evaluationToolDto = new EvaluationToolDto();
        evaluationToolDto.setId(evaluationToolEntity.getId());
        evaluationToolDto.setName(evaluationToolEntity.getName());
        evaluationToolDto.setDescription(evaluationToolEntity.getDescription());
        return evaluationToolDto;
    }

    public EvaluationToolDto entityToBO(EvaluationToolTemplateEntity evaluationToolTemplateEntity){
        EvaluationToolDto evaluationToolDto = new EvaluationToolDto();
        evaluationToolDto.setId(evaluationToolTemplateEntity.getId());
        evaluationToolDto.setName(evaluationToolTemplateEntity.getName());
        evaluationToolDto.setDescription(evaluationToolTemplateEntity.getDescription());
        return evaluationToolDto;
    }

    public EvaluationScaleDto entityToBO(RatingScaleEntity ratingScaleEntity){
        EvaluationScaleDto evaluationScaleDto = new EvaluationScaleDto();
        evaluationScaleDto.setId(ratingScaleEntity.getId());
        evaluationScaleDto.setName(ratingScaleEntity.getName());
        evaluationScaleDto.setDescription(ratingScaleEntity.getDescription());
        for(RatingValueEntity ratingValueEntity: ratingScaleEntity.getRatingValueList()){
            evaluationScaleDto.addEvaluationValueList(entityToBO(ratingValueEntity));
        }
        return evaluationScaleDto;
    }

    public EvaluationScaleDto entityToBO(EvaluationScaleTemplateEntity evaluationScaleTemplateEntity){
        EvaluationScaleDto evaluationScaleDto = new EvaluationScaleDto();
        evaluationScaleDto.setId(evaluationScaleTemplateEntity.getId());
        evaluationScaleDto.setName(evaluationScaleTemplateEntity.getName());
        evaluationScaleDto.setDescription(evaluationScaleTemplateEntity.getDescription());
        for(EvaluationValueTemplateEntity evaluationValueTemplateEntity: evaluationScaleTemplateEntity.getEvaluationValueList()){
            evaluationScaleDto.addEvaluationValueList(entityToBO(evaluationValueTemplateEntity));
        }
        return evaluationScaleDto;
    }

    public EvaluationValueDto entityToBO(RatingValueEntity ratingValueEntity){
        EvaluationValueDto evaluationValueDto = new EvaluationValueDto();
        evaluationValueDto.setId(ratingValueEntity.getId());
        evaluationValueDto.setText(ratingValueEntity.getQualification().getText());
        evaluationValueDto.setValue(ratingValueEntity.getQualification().getValue());
        evaluationValueDto.setWeight(ratingValueEntity.getQualification().getWeight());
        return evaluationValueDto;
    }

    public EvaluationValueDto entityToBO(EvaluationValueTemplateEntity evaluationValueTemplateEntity){
        EvaluationValueDto evaluationValueDto = new EvaluationValueDto();
        evaluationValueDto.setId(evaluationValueTemplateEntity.getId());
        evaluationValueDto.setText(evaluationValueTemplateEntity.getQualification().getText());
        evaluationValueDto.setValue(evaluationValueTemplateEntity.getQualification().getValue());
        evaluationValueDto.setWeight(evaluationValueTemplateEntity.getQualification().getWeight());
        return evaluationValueDto;
    }

    public EvaluationDto entityToBO(EvaluationEntity evaluationEntity){
        EvaluationDto evaluationDto = new EvaluationDto();
        evaluationDto.setId(evaluationEntity.getId());
        evaluationDto.setObjective(evaluationEntity.getObjective());
        evaluationDto.setDate(validation.dateToString(evaluationEntity.getDate()));
        evaluationDto.setWeight(evaluationEntity.getWeight());
        evaluationDto.setEvaluationTool(entityToBO(evaluationEntity.getEvaluationTool()));
        evaluationDto.setEvaluationScale(entityToBO(evaluationEntity.getRatingScale()));
        evaluationDto.setStatus(evaluationEntity.getStatus().name());
        evaluationDto.setEvaluationPlanId(evaluationEntity.getEvaluationPlan().getId());
        if(evaluationEntity.getQualificationUserList() != null) {
            List<QualificationUserDto> qualificationUserDtoList = new ArrayList<QualificationUserDto>();
            for (QualificationUserEntity qualificationUserEntity : evaluationEntity.getQualificationUserList()) {
                qualificationUserDtoList.add(entityToBO(qualificationUserEntity));
            }
            evaluationDto.setQualificationUserList(qualificationUserDtoList);
        }
        return evaluationDto;
    }

    public EvaluationDto entityToBO(EvaluationTemplateEntity evaluationTemplateEntity){
        EvaluationDto evaluationDto = new EvaluationDto();
        evaluationDto.setId(evaluationTemplateEntity.getId());
        evaluationDto.setObjective(evaluationTemplateEntity.getObjective());
        evaluationDto.setDate(validation.dateToString(evaluationTemplateEntity.getDate()));
        evaluationDto.setWeight(evaluationTemplateEntity.getWeight());
        evaluationDto.setEvaluationTool(entityToBO(evaluationTemplateEntity.getEvaluationTool()));
        evaluationDto.setEvaluationScale(entityToBO(evaluationTemplateEntity.getEvaluationScale()));
        evaluationDto.setStatus(evaluationTemplateEntity.getStatus().name());
        evaluationDto.setEvaluationPlanId(evaluationTemplateEntity.getEvaluationPlanTemplate().getId());

        return evaluationDto;
    }

    public QualificationUserDto entityToBO(QualificationUserEntity qualificationUserEntity){
        QualificationUserDto qualificationUserDto = new QualificationUserDto();
        qualificationUserDto.setId(qualificationUserEntity.getId());
        qualificationUserDto.setUser(entityToBO(qualificationUserEntity.getStudent()));
        qualificationUserDto.setNameEvaluation(qualificationUserEntity.getEvaluation().getEvaluationTool().getName());
        qualificationUserDto.setDateEvaluation(validation.dateToString(qualificationUserEntity.getEvaluation().getDate()));
        qualificationUserDto.setMatter(entityToBO(qualificationUserEntity.getMatter()));
        if(qualificationUserEntity.getQualification() != null) {
            qualificationUserDto.setQualification(new QualificationDto(qualificationUserEntity.getQualification().getValue(),
                    qualificationUserEntity.getQualification().getText(), qualificationUserEntity.getQualification().getWeight()));
        }
        return qualificationUserDto;
    }

    public EvaluationPlanDto entityToBO(EvaluationPlanEntity evaluationPlanEntity){
        if(evaluationPlanEntity == null)
            return null;
        EvaluationPlanDto evaluationPlanDto = new EvaluationPlanDto();
        evaluationPlanDto.setId(evaluationPlanEntity.getId());
        evaluationPlanDto.setName(evaluationPlanEntity.getName());
        if(evaluationPlanEntity.getMatter() != null) {
            MatterCommunitySectionDto matterCommunitySectionDto = new MatterCommunitySectionDto();
            matterCommunitySectionDto.setId(evaluationPlanEntity.getMatter().getId());
            matterCommunitySectionDto.setTeacherId(evaluationPlanEntity.getMatter().getUser().getId());
            matterCommunitySectionDto.setType(evaluationPlanEntity.getMatter().getType().name());
            if (evaluationPlanEntity.getMatter().getMatterCommunity() != null)
                matterCommunitySectionDto.setMatterCommunity(entityToBO(evaluationPlanEntity.getMatter().getMatterCommunity()));
            if (evaluationPlanEntity.getMatter().getSection() != null)
                matterCommunitySectionDto.setSection(entityToBO(evaluationPlanEntity.getMatter().getSection()));
            evaluationPlanDto.setMatterCommunitySection(matterCommunitySectionDto);
        }
        evaluationPlanDto.setUserId(evaluationPlanEntity.getUser().getId());
        return evaluationPlanDto;
    }

    public EvaluationPlanDto entityToBO(EvaluationPlanTemplateEntity evaluationPlanTemplateEntity){
        if(evaluationPlanTemplateEntity == null)
            return null;
        EvaluationPlanDto evaluationPlanDto = new EvaluationPlanDto();
        evaluationPlanDto.setId(evaluationPlanTemplateEntity.getId());
        evaluationPlanDto.setName(evaluationPlanTemplateEntity.getName());
        evaluationPlanDto.setUserId(evaluationPlanTemplateEntity.getUser().getId());
        return evaluationPlanDto;
    }


    public EvaluationPlanDto entityToBOFull(EvaluationPlanEntity evaluationPlanEntity){
        if(evaluationPlanEntity == null)
            return null;
        EvaluationPlanDto evaluationPlanDto = new EvaluationPlanDto();
        evaluationPlanDto.setId(evaluationPlanEntity.getId());
        evaluationPlanDto.setName(evaluationPlanEntity.getName());
        evaluationPlanDto.setUserId(evaluationPlanEntity.getUser().getId());
        if(evaluationPlanEntity.getMatter() != null) {
            MatterCommunitySectionDto matterCommunitySectionDto = new MatterCommunitySectionDto();
            matterCommunitySectionDto.setId(evaluationPlanEntity.getMatter().getId());
            matterCommunitySectionDto.setTeacherId(evaluationPlanEntity.getMatter().getUser().getId());
            matterCommunitySectionDto.setType(evaluationPlanEntity.getMatter().getType().name());
            if (evaluationPlanEntity.getMatter().getMatterCommunity() != null)
                matterCommunitySectionDto.setMatterCommunity(entityToBO(evaluationPlanEntity.getMatter().getMatterCommunity()));
            if (evaluationPlanEntity.getMatter().getSection() != null)
                matterCommunitySectionDto.setSection(entityToBO(evaluationPlanEntity.getMatter().getSection()));
            evaluationPlanDto.setMatterCommunitySection(matterCommunitySectionDto);
        }
        if(evaluationPlanEntity.getEvaluationList() != null) {
            for(EvaluationEntity evaluationEntity: evaluationPlanEntity.getEvaluationList())
                evaluationPlanDto.addEvaluations(this.entityToBO(evaluationEntity));
        }
        return evaluationPlanDto;
    }

    public EvaluationPlanDto entityToBOFull(EvaluationPlanTemplateEntity evaluationPlanTemplateEntity){
        if(evaluationPlanTemplateEntity == null)
            return null;
        EvaluationPlanDto evaluationPlanDto = new EvaluationPlanDto();
        evaluationPlanDto.setId(evaluationPlanTemplateEntity.getId());
        evaluationPlanDto.setName(evaluationPlanTemplateEntity.getName());
        evaluationPlanDto.setUserId(evaluationPlanTemplateEntity.getUser().getId());
        if(evaluationPlanTemplateEntity.getEvaluationTemplateList() != null) {
            for(EvaluationTemplateEntity evaluationTemplateEntity: evaluationPlanTemplateEntity.getEvaluationTemplateList())
                evaluationPlanDto.addEvaluations(this.entityToBO(evaluationTemplateEntity));
        }
        return evaluationPlanDto;
    }

    public WizardDto entityToBO(WizardEntity wizardEntity){
        if(wizardEntity == null)
            return null;
        WizardDto wizardDto = new WizardDto();
        wizardDto.setUserId(wizardEntity.getUser().getId());
        if(wizardEntity.getCommunity() != null)
            wizardDto.setCommunity(entityToBO(wizardEntity.getCommunity()));
        if(wizardEntity.getMatterCommunitySection() != null)
            wizardDto.setMatterCommunitySection(entityToBO(wizardEntity.getMatterCommunitySection()));
        if(wizardEntity.getEvaluationPlan() != null)
            wizardDto.setEvaluationPlan(entityToBOFull(wizardEntity.getEvaluationPlan()));
        if(wizardEntity.getUserProfile() != 0){
            wizardDto.setProfileUser(1);
        }
        return wizardDto;
    }

    public UserDto entityToBOFull(UserEntity userEntity) {
        UserDto userDto = new UserDto();
        userDto.setId(userEntity.getId());
        userDto.setIdentification(userEntity.getIdentification());
        userDto.setFirstName(userEntity.getFirstName());
        userDto.setSecondName(userEntity.getSecondName());
        userDto.setLastName(userEntity.getLastName());
        userDto.setSecondLastName(userEntity.getSecondLastName());
        userDto.setMail(userEntity.getMail());
        userDto.setStatus(userEntity.getStatus().name());
        if (userEntity.getBirthdate() != null)
            userDto.setBirthdate(validation.dateToString(userEntity.getBirthdate()));
        if (userEntity.getGender() != null)
            userDto.setGender(userEntity.getGender().name());
        if (userEntity.getCurriculumList().size() > 0){
            for(CurriculumEntity curriculumEntity: userEntity.getCurriculumList()){
                userDto.addCurriculum(entityToBO(curriculumEntity));
            }
        }
        /*
        if (userEntity.getRoleList().size() > 0){
            for (RoleEntity roleEntity : userEntity.getRoleList()) {
                userDto.addRoleList(entityToBO(roleEntity));
            }
        }
        */
        if(userEntity.getFoto() != null){
            userDto.setFoto(userEntity.getFoto());
        }
        return userDto;
    }

    public NetworksEntity boToEntity(NetworksDto networksDto){
        NetworksEntity networksEntity = new NetworksEntity();

        networksEntity.setId(networksDto.getId());
        networksEntity.setNetworks(networksDto.getNetwork());

        return networksEntity;
    }

    public NetworksDto entityToBO(NetworksEntity networksEntity){
        NetworksDto networksDto = new NetworksDto();

        networksDto.setId(networksEntity.getId());
        networksDto.setNetwork(networksEntity.getNetworks());

        return networksDto;
    }

}
