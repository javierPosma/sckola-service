package com.posma.sckola.config;

import com.posma.sckola.SkolaApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;

@ServletComponentScan(value="com.posma.skola.app.security")
@PropertySource(value = {"classpath:application.properties"})
public class ServletInitializer extends SpringBootServletInitializer {


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SkolaApplication.class);
    }




}
